#include <loft.h>
#include <stdlib.h>
#include <string.h>

LoftWindow win;
    LoftLayout lt1;
        LoftScrollView sv1;
            LoftBuffer buffer;
            LoftTextTagTable tt;
            LoftEditable e;

            LoftTextTagTable tt;
                LoftTextTag t;

        LoftLabel lb1;
        LoftLabel lb2;

        LoftScale sc;
            LoftAdjustment adj;

        LoftLayout lt3;
            LoftButton btn;
            LoftSwitch sw;
            LoftButton pbtn;

char* lb1_str = "Have <u><i>you</i></u> derped today?";
char* lb2_str = "This is a demo of <b>Loft</b>.\n\n"
                "CTRL-b: toggle layout\n"
                "CTRL-t: toggle text tags\n"
                "CTRL-q: quit";

char* buffer_text = "This is some example text in a LoftBuffer object "
                    "attached to a LoftEditable inside of a LoftScrollView.\n\n"
                    "Below is another LoftScrollView with a LoftLayout "
                    "containing four LoftLabel widgets, and below it a "
                    "LoftScale with an attached LoftAdjustment and at the bottom "
                    "a horizontal LoftLayout with two LoftButton widgets "
                    "and a LoftSwitch.";

int _window_on_delete (LoftObject* obj, void* arg, void* data) {
    loft_break();
    return 1;
}

int _window_on_key_press (LoftObject* obj, LoftKey* k, void* data) {
    if (k->state & ControlMask) {
        if (k->sym == XK_b) {
            if (lt3.w.visible)
                loft_widget_hide(&lt3.w);
            else
                loft_widget_show(&lt3.w);
            return 1;
        }
        else if (k->sym == XK_t) {
            if (e.tag_table == NULL)
                loft_editable_set_tag_table(&e, &tt);
            else
                loft_editable_set_tag_table(&e, NULL);
            return 1;
        }
        else if (k->sym == XK_a) {
            loft_scale_set_aspect(&sc, ~loft_scale_get_aspect(&sc));
            loft_widget_set_attrs(&sc.w, sc.w.attrs == EXPAND_X ? EXPAND_Y : EXPAND_X);
            return 1;
        }
        else if (k->sym == XK_q) {
            loft_break();
            return 1;
        }
    }

    return 0;
}

int _pbtn_on_set_active (LoftButton* pbtn, bool* active, void* data) {
    loft_button_set_pulse(&btn, *active);
    return 0;
}

int _switch_on_set_active (LoftSwitch* sw, bool* active, void* data) {
    loft_scale_set_inverted(&sc, *active);
    return 0;
}

int main (int argc, char** argv) {
    loft_init();

    loft_window_init(&win, "Loft Demo", 5, false);
    loft_layout_init(&lt1, ASPECT_V, 5);

    loft_label_init(&lb1, lb1_str, true);
    loft_scrollv_init(&sv1, SCROLL_Y);

    loft_buffer_init(&buffer, buffer_text, -1);
    loft_editable_init(&e, &buffer, "This is pretext..");
    loft_text_tag_table_init(&tt);
    loft_text_tag_init(&t, ATTR_FOREGROUND);

    loft_label_init(&lb2, lb2_str, true);

    loft_adj_init(&adj);
    loft_scale_init(&sc, ASPECT_H, &adj);

    loft_layout_init(&lt3, ASPECT_H, 5);
    loft_button_init(&btn, "Herp", false);
    loft_switch_init(&sw, SWITCH_FANCY);
    loft_button_init(&pbtn, "Derp", false);

//  setup widgets
    loft_window_set_dialog(&win, true);

    if (argc > 1 && strcmp(argv[1], "-no-base") == 0)
       loft_widget_set_drawable(&win.w, false);

    loft_label_set_alignment(&lb1, ALIGN_CENTER);
    loft_label_set_draw_bg(&lb1, true);
    loft_label_set_bg_slant(&lb1, 6);

    loft_editable_set_line_wrap(&e, true);
    rgba_from_str(&t.foreground, "#FF0000");
    loft_text_tag_table_add_tag(&tt, &t);
    loft_text_tag_table_set_range(&tt, &t, 0, 4);
    loft_editable_set_tag_table(&e, &tt);

    loft_label_set_highlighted(&lb2, true);
    loft_label_set_word_wrap(&lb2, true);

    loft_adj_set_min(&adj, 20);
    loft_adj_set_max(&adj, 120);
    loft_adj_set_value(&adj, 70);

    loft_button_set_activatable(&btn, true);
    loft_button_set_activatable(&pbtn, true);

//  set widget attributes
    loft_widget_set_attrs(&lt1.w, EXPAND);
    loft_widget_set_attrs(&lb1.w, EXPAND_X);

    loft_widget_set_attrs(&lb2.w, EXPAND_X);

    loft_widget_set_attrs(&sv1.w, EXPAND);
    loft_widget_set_attrs(&e.w, EXPAND);

    loft_widget_set_attrs(&sc.w, EXPAND_X);

    loft_widget_set_attrs(&lt3.w, EXPAND_X);
    loft_widget_set_attrs(&btn.w, EXPAND_X|PACK_X|FLOW_L);
    loft_widget_set_attrs(&sw.w, FLOW_D);
    loft_widget_set_attrs(&pbtn.w, EXPAND_X|PACK_X|FLOW_R);

//  attach widgets to parents
    loft_widget_attach(&lt1.w, &win.w, -1);
    loft_widget_attach(&lb1.w, &lt1.w, -1);

    loft_widget_attach(&lb2.w, &lt1.w, -1);

    loft_widget_attach(&sv1.w, &lt1.w, -1);
    loft_widget_attach(&e.w, &sv1.w, -1);

    loft_widget_attach(&sc.w, &lt1.w, -1);
    loft_widget_attach(&lt3.w, &lt1.w, -1);
    loft_widget_attach(&btn.w, &lt3.w, -1);
    loft_widget_attach(&sw.w, &lt3.w, -1);
    loft_widget_attach(&pbtn.w, &lt3.w, -1);

//  connect signals
    loft_connect(&win.w.obj, "delete", _window_on_delete, NULL);
    loft_connect(&win.w.obj, "key-press", _window_on_key_press, NULL);
    loft_connect(&pbtn.w.obj, "set-active", _pbtn_on_set_active, NULL);
    loft_connect(&sw.w.obj, "set-active", _switch_on_set_active, NULL);

//  set preferred sizes
    loft_widget_set_pref_size(&win.w, 300, 375);
    loft_widget_set_pref_size(&sv1.w, -1, 100);

//  configure window
    loft_window_show_all(&win);

//  start
    loft_main(NULL);
}
