#include <loft.h>

LoftWindow win;
LoftLayout lt;
LoftLabel lb1, lb2;
LoftButton btn;

int main (int argc, char** argv) {
    loft_init();

    loft_window_init(&win, "Loft Label Test", 5, false);
    loft_layout_init(&lt, ASPECT_H, 5);

    loft_label_init(&lb1, "I don't wan't to live on this planet anymore", false);
    loft_label_init(&lb2, "I don't wan't to live on this planet anymore", false);
    loft_label_set_word_wrap(&lb1, true);
    loft_label_set_word_wrap(&lb2, true);

    loft_button_init(&btn, "Lonely Button", false);

    loft_widget_set_attrs(&lt.w, EXPAND);
    loft_widget_set_attrs(&lb1.w, EXPAND_X);
    loft_widget_set_attrs(&lb2.w, EXPAND_X);

    loft_widget_attach(&lb1.w, &lt.w, -1);
    loft_widget_attach(&btn.w, &lt.w, -1);
    loft_widget_attach(&lb2.w, &lt.w, -1);
    loft_widget_attach(&lt.w, &win.w, -1);

    loft_window_show_all(&win);
    loft_main(NULL);
}
