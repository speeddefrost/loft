#include "loft.h"
#include <clip/range.h>

char *ADJUSTMENT = "loft-adjustment";

double loft_adj_get_max (LoftAdjustment *adj) {
  return adj->max;
}

double loft_adj_get_min (LoftAdjustment *adj) {
  return adj->min;
}

double loft_adj_get_perc (LoftAdjustment *adj) {
  return range_to_perc(adj->value, adj->min, adj->max);
}

double loft_adj_get_perc_bounded (LoftAdjustment *adj) {
  if(adj->value < adj->min)
    return 0;
  else if(adj->value > adj->max)
    return 1;
  else
    return range_to_perc(adj->value, adj->min, adj->max);
}

double loft_adj_get_value (LoftAdjustment *adj) {
  return adj->value;
}

double loft_adj_get_value_bounded (LoftAdjustment *adj) {
  if(adj->value < adj->min)
    return adj->min;
  else if(adj->value > adj->max)
    return adj->max;
  else
    return adj->value;
}

void loft_adj_init (LoftAdjustment *adj) {
  loft_object_init(&adj->obj, ADJUSTMENT);
  adj->min = 0;
  adj->max = 0;
  adj->value = 0;
}

void loft_adj_set_max (LoftAdjustment *adj, double max) {
  if(max == adj->max)
    return;

  adj->max = max;

  loft_emit(&adj->obj, "changed", NULL);
  loft_emit(&adj->obj, "max-changed", &adj->max);
}

void loft_adj_set_min (LoftAdjustment *adj, double min) {
  if(min == adj->min)
    return;

  adj->min = min;

  loft_emit(&adj->obj, "changed", NULL);
  loft_emit(&adj->obj, "min-changed", &adj->min);
}

void loft_adj_set_perc (LoftAdjustment *adj, double perc) {
  loft_adj_set_value(adj, perc_to_range(perc, adj->min, adj->max));
}

void loft_adj_set_perc_bounded (LoftAdjustment *adj, double perc) {
  if(perc > 1)
    perc = 1;
  else if(perc < 0)
    perc = 0;

  loft_adj_set_perc(adj, perc);
}

void loft_adj_set_range (LoftAdjustment *adj, double min, double max) {
  int diff = (min != adj->min) | ((max != adj->max) << 1);
  if(diff == 0)
    return;

  if(diff & 1) {
    adj->min = min;
    loft_emit(&adj->obj, "min-changed", &adj->min);
  }

  if(diff & 2) {
    adj->max = max;
    loft_emit(&adj->obj, "max-changed", &adj->max);
  }

  loft_emit(&adj->obj, "changed", NULL);
}

void loft_adj_set_value (LoftAdjustment *adj, double value) {
  if(value == adj->value)
    return;

  adj->value = value;

  loft_emit(&adj->obj, "changed", NULL);
  loft_emit(&adj->obj, "value-changed", &adj->value);
}

void loft_adj_set_value_bounded (LoftAdjustment *adj, double value) {
  if(value > adj->max)
    value = adj->max;
  else if(value < adj->min)
    value = adj->min;

  loft_adj_set_value(adj, value);
}
