#include "loft.h"
#include <clip/utf8.h>
#include <string.h>

#define CHUNK_LEN 24

char *BUFFER = "loft-buffer";

void _buffer_update_lines (LoftBuffer *b) {
  if(b->data == NULL) {
    if(b->lines != NULL)
      free(b->lines);

    b->lines = NULL;
    b->n_lines = 0;

    return;
  }

  int i, byte_count;
  int n_lines = 1;

  for(i = 0; i < b->len; i += byte_count) {
    byte_count = utf8_byte_count(b->data[i]);
    if(b->data[i] == '\n' || b->data[i] == '\r')
      ++n_lines;
  }

  if(n_lines != b->n_lines) {
    LoftBufferLine *tmp = realloc(b->lines, sizeof(LoftBufferLine) * n_lines);
    if(tmp == NULL)
      return;

    b->lines = tmp;
    b->n_lines = n_lines;
  }

  b->lines[0] = (LoftBufferLine) { 0, 0, 0, b->data };

  int j, li = 0;
  for(i = 0, j = 0; i < b->len; i += byte_count, ++j) {
    byte_count = utf8_byte_count(b->data[i]);

    if(byte_count == 1 && (b->data[i] == '\n' || b->data[i] == '\r')) {
      ++li;
      b->lines[li] = (LoftBufferLine) { li, 0, j + 1, b->data + i + 1 };
      continue;
    }

    ++b->lines[li].len;
  }
}

int _buffer_on_deinit (LoftBuffer *b, void *arg, void *data) {
  loft_buffer_clear(b);
  return 0;
}

// --

void loft_buffer_clear (LoftBuffer *b) {
  if(b->data != NULL)
    free(b->data);

  b->data = NULL;
  b->len = 0;
  b->alloc_len = 0;

  _buffer_update_lines(b);

  loft_emit(&b->obj, "clear", NULL);
  loft_emit(&b->obj, "changed", NULL);
}

void loft_buffer_delete (LoftBuffer *b, int start, int end) {
  if(b->len == 0)
    return;

  if(start < 0 || start >= b->len)
    start = b->len - 1;

  if(end < 0 || end >= b->len)
    end = b->len - 1;

  int offset = (end - start) + 1;

  int i;
  int max = b->len - offset;

  for(i = start; i <= max; ++i)
    b->data[i] = b->data[i + offset];

  b->len -= offset;
  _buffer_update_lines(b);

  struct range r = { start, end };
  loft_emit(&b->obj, "delete", &r);
  loft_emit(&b->obj, "changed", NULL);
}

void loft_buffer_init (LoftBuffer *b, char *data, int len) {
  loft_object_init(&b->obj, BUFFER);
  loft_connect(&b->obj, "deinit", _buffer_on_deinit, NULL);

  b->data = NULL;
  b->len = 0;
  b->alloc_len = 0;
  b->lines = NULL;
  b->n_lines = 0;

  if(data != NULL)
    loft_buffer_set(b, data, len);
}

int loft_buffer_insert (LoftBuffer *b, int pos, char *data, int len) {
  if(loft_emit(&b->obj, "insert", data) == 1)
    return 0;

  if(pos < 0 || pos > b->len)
    pos = b->len;

  if(len < 0)
    len = strlen(data);

  if(len == 0)
    return 0;

  if(b->data == NULL) {
    b->data = strdup(data);
    b->len = len;
    b->alloc_len = b->len;

    goto insert_finalize;
  }

  int new_len = b->len + len;

  if(new_len > b->alloc_len) {
    int new_alloc_len = new_len + CHUNK_LEN;
    char *tmp = realloc(b->data, new_alloc_len + 1);
    if(tmp == NULL)
      return -1;

    b->data = tmp;
    b->alloc_len = new_alloc_len;
  }

  int i;
  for(i = b->len; i >= pos; --i)
    b->data[i + len] = b->data[i];

  int j;
  for(i = pos, j = 0; j < len; ++i, ++j)
    b->data[i] = data[j];

  b->len = new_len;

insert_finalize:

  _buffer_update_lines(b);
  loft_emit(&b->obj, "changed", NULL);

  return 1;
}

LoftBufferLine *loft_buffer_line_from_offset (LoftBuffer *b, int offset) {
  int i;
  LoftBufferLine *ln;
  for(i = 0; i < b->n_lines; ++i) {
    ln = &b->lines[i];
    if(offset >= ln->offset && offset <= (ln->offset + ln->len))
      return ln;
  }
  return NULL;
}

void loft_buffer_set (LoftBuffer *b, char *data, int len) {
  if(data == NULL || len == 0) {
    loft_buffer_clear(b);
    return;
  }

  if(b->data != NULL)
    free(b->data);

  if(len < 0)
    len = strlen(data);

  b->data = strdup(data);
  b->len = len;
  b->alloc_len = b->len;

  _buffer_update_lines(b);
  loft_emit(&b->obj, "changed", NULL);
}
