#include "loft.h"
#include <clip/string.h>

char *BUTTON = "loft-button";

void _button_update_layout (LoftButton *btn) {
  if(btn->use_markup)
    pango_layout_set_markup(btn->_lt, btn->str, -1);
  else
    pango_layout_set_text(btn->_lt, btn->str, -1);
}

// --

int _button_on_deinit (LoftButton *btn, void *arg, void *data) {
  loft_timer_stop(&btn->_pulse_timer);
  g_object_unref(btn->_lt);
  return 0;
}

int _button_on_draw (LoftWidget *w, cairo_t *cr, void *data) {
  LoftButton *btn = (LoftButton*) w;

  int state;
  if(w->sensitive) {
    if(btn->active)
      state = ACTIVE;
    else
      state = NORMAL;
  }
  else {
    state = INSENSITIVE;
  }

  int pair;
  if(w->pressed)
    pair = SELECTED;
  else if(w->prelight)
    pair = PRELIGHT;
  else
    pair = IDLE;

  struct rgba_pair fade_p;
  struct rgba_pair *p;

  if(btn->pulse && btn->_draw_pulse) {
    rgba_fade (
        &fade_p.bg,
        &w->colors[state][pair].bg, &loft.colors.urgency[state][HIGH].bg,
        btn->pulse_fade[0], btn->pulse_fade[1]
        );

    rgba_fade (
        &fade_p.fg,
        &w->colors[state][pair].fg, &loft.colors.urgency[state][HIGH].fg,
        btn->pulse_fade[0], btn->pulse_fade[1]
        );

    p = &fade_p;
  }
  else {
    p = &w->colors[state][pair];
  }

  rgba_cairo_set(cr, &p->bg);
  cairo_rectangle(cr, 0, 0, w->width, w->height);
  cairo_fill(cr);

  if(btn->str != NULL) {
    int th;
    pango_layout_get_pixel_size(btn->_lt, NULL, &th);

    rgba_cairo_set(cr, &p->fg);
    cairo_move_to(cr, btn->padding, (w->height - th) / 2);

    pango_cairo_update_layout(cr, btn->_lt);
    pango_cairo_show_layout(cr, btn->_lt);
  }

  return 0;
}

int _button_on_pointer_enter (LoftWidget *w, void *arg, void *data) {
  if(w->sensitive == false)
    return 0;

  loft_widget_refresh_fast(w);
  return 0;
}

int _button_on_pointer_leave (LoftWidget *w, void *arg, void *data) {
  if(w->sensitive == false)
    return 0;

  loft_widget_refresh_fast(w);
  return 0;
}

int _button_on_press (LoftButton *btn, void *arg, void *data) {
  loft_widget_refresh_fast(&btn->w);
  return 0;
}

int _button_on_release (LoftButton *btn, void *arg, void *data) {
  if(btn->activatable) {
    btn->active ^= true;
    loft_emit(&btn->w.obj, btn->active ? "activate" : "deactivate", NULL);
    loft_emit(&btn->w.obj, "set-active", &btn->active);
  }

  loft_widget_refresh_fast(&btn->w);
  loft_emit(&btn->w.obj, "toggle", NULL);

  return 0;
}

int _button_on_size_request (LoftButton *btn, struct wh *size_req, void *data) {
  int p2 = btn->padding * 2;
  int min_w = p2,
      min_h = p2;

  if(btn->str != NULL) {
    int tw,th;
    pango_layout_set_width(btn->_lt, -1);
    pango_layout_get_pixel_size(btn->_lt, &tw, &th);

    min_w += tw;
    min_h += th;
  }

  loft_widget_set_min_size(&btn->w, min_w, min_h);
  return 0;
}

int _button_on_pulse_timeout (LoftTimer *t, LoftButton *btn) {
  btn->_draw_pulse ^= true;
  loft_widget_refresh_fast(&btn->w);
  return 0;
}

// --

int loft_button_get_alignment (LoftButton *btn) {
  return btn->alignment;
}

int loft_button_get_padding (LoftButton *btn) {
  return btn->padding;
}

double *loft_button_get_pulse_fade (LoftButton *btn) {
  return (double*) btn->pulse_fade;
}

int loft_button_get_pulse_timeout (LoftButton *btn) {
  return btn->pulse_timeout;
}

void loft_button_init (LoftButton *btn, char *str, bool use_markup) {
  loft_widget_init(&btn->w, BUTTON, true, true, SIZE_STATIC);

  loft_connect(&btn->w.obj, "deinit", _button_on_deinit, NULL);
  loft_connect(&btn->w.obj, "draw", _button_on_draw, NULL);
  loft_connect(&btn->w.obj, "button-press", _button_on_press, NULL);
  loft_connect(&btn->w.obj, "button-release", _button_on_release, NULL);
  loft_connect(&btn->w.obj, "pointer-enter", _button_on_pointer_enter, NULL);
  loft_connect(&btn->w.obj, "pointer-leave", _button_on_pointer_leave, NULL);
  loft_connect(&btn->w.obj, "size-request", _button_on_size_request, NULL);

  btn->alignment = ALIGN_CENTER;
  btn->padding = 4;

  btn->activatable = false;
  btn->active = false;
  btn->pulse = false;
  btn->use_markup = use_markup;

  btn->str = str;

  btn->pulse_fade[0] = 5;
  btn->pulse_fade[1] = 10;
  btn->pulse_timeout = 500;

  //  --

  cairo_t *cr = cairo_create(btn->w.buffer);
  btn->_lt = pango_cairo_create_layout(cr);
  cairo_destroy(cr);

  pango_layout_set_auto_dir(btn->_lt, true);
  pango_layout_set_font_description(btn->_lt, loft.font.desc);

  _button_update_layout(btn);

  btn->_draw_pulse = false;
  loft_timer_init(&btn->_pulse_timer, btn->pulse_timeout, _button_on_pulse_timeout, btn);
}

bool loft_button_is_activatable (LoftButton *btn) {
  return btn->activatable;
}

bool loft_button_is_active (LoftButton *btn) {
  return btn->active;
}

LoftButton *loft_button_new (char *str, bool use_markup) {
  LoftButton *btn = malloc(sizeof(LoftButton));
  if(btn == NULL)
    return NULL;

  loft_button_init(btn, str, use_markup);
  loft_object_set_freeable(&btn->w.obj, true);

  return btn;
}

void loft_button_set_active (LoftButton *btn, bool active) {
  if(btn->activatable) {
    btn->active = active;
    loft_emit(&btn->w.obj, "set-active", &btn->active);
    loft_widget_refresh(&btn->w, true);
  }
}

void loft_button_set_activatable (LoftButton *btn, bool activatable) {
  btn->activatable = activatable;
  if(btn->activatable == false && btn->active) {
    btn->active = false;
    loft_widget_refresh(&btn->w, true);
  }
}

void loft_button_set_alignment (LoftButton *btn, int alignment) {
  btn->alignment = alignment;
  pango_layout_set_alignment(btn->_lt, btn->alignment);
  loft_widget_refresh(&btn->w, true);
}

void loft_button_set_padding (LoftButton *btn, int padding) {
  btn->padding = padding;
  loft_widget_refresh(&btn->w, true);
}

void loft_button_set_pulse (LoftButton *btn, bool pulse) {
  btn->pulse = pulse;
  loft_emit(&btn->w.obj, "set-pulse", &btn->pulse);

  if(btn->pulse) {
    loft_timer_start(&btn->_pulse_timer);
  }
  else {
    loft_timer_stop(&btn->_pulse_timer);
    if(btn->_draw_pulse) {
      btn->_draw_pulse = false;
      loft_widget_refresh_fast(&btn->w);
    }
  }
}

void loft_button_set_pulse_fade (LoftButton *btn, double value, double max) {
  btn->pulse_fade[0] = value;
  btn->pulse_fade[1] = max;
}

void loft_button_set_pulse_timeout (LoftButton *btn, int pulse_timeout) {
  btn->pulse_timeout = pulse_timeout;
  loft_timer_set_timeout(&btn->_pulse_timer, btn->pulse_timeout);
}

void loft_button_set_str (LoftButton *btn, char *str, bool use_markup) {
  btn->str = str;
  btn->use_markup = use_markup;

  _button_update_layout(btn);

  loft_emit(&btn->w.obj, "set-str", btn->str);
  loft_widget_refresh(&btn->w, true);
}
