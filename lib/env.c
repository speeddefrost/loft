#include "loft.h"
#include <clip/filesystem.h>
#include <clip/string.h>
#include <locale.h>
#include <sys/stat.h>
#include <X11/Xutil.h>
#include <X11/extensions/Xrender.h>

LoftEnv loft;

char *color_keys [N_STATES][N_PAIRS][2] = {
  {
    { "colors.static.bg",        "colors.static.fg"        },
    { "colors.prelight.bg",      "colors.prelight.fg"      },
    { "colors.prime.bg",         "colors.prime.fg"         }
  },

  {
    { "colors.alt_static.bg",    "colors.alt_static.fg"    },
    { "colors.alt_prelight.bg",  "colors.alt_prelight.fg"  },
    { "colors.alt_prime.bg",     "colors.alt_prime.fg"     }
  },

  {
    { "colors.ins_static.bg",    "colors.ins_static.fg"    },
    { "colors.ins_prelight.bg",  "colors.ins_prelight.fg"  },
    { "colors.ins_prime.bg",     "colors.ins_prime.fg"     }
  }
};

char *default_colors [N_STATES][N_PAIRS][2] = {
  { /* normal */
    { "#262626E8", "#999999" },
    { "#333333E8", "#B3B3B3" },
    { "#3D5866E8", "#B3B3B3" }
  },

  { /* alternate */
    { "#52663DE8", "#B3B3B3" },
    { "#5C7345E8", "#CCCCCC" },
    { "#66523DE8", "#CCCCCC" }
  },

  { /* insensitive */
    { "#1A1A1AE8", "#4D4D4D" },
    { "#1A1A1AE8", "#4D4D4D" },
    { "#293033E8", "#4D4D4D" }
  }
};

char *urgency_color_keys [N_STATES][N_UPAIRS][2] = {
  {
    { "colors.urgency_low.bg",        "colors.urgency_low.fg"        },
    { "colors.urgency_medium.bg",     "colors.urgency_medium.fg"     },
    { "colors.urgency_high.bg",       "colors.urgency_high.fg"       }
  },

  {
    { "colors.alt_urgency_low.bg",    "colors.urgency_low.fg"        },
    { "colors.alt_urgency_medium.bg", "colors.urgency_medium.fg"     },
    { "colors.alt_urgency_high.bg",   "colors.urgency_high.fg"       }
  },

  {
    { "colors.ins_urgency_low.bg",    "colors.ins_urgency_low.fg"    },
    { "colors.ins_urgency_medium.bg", "colors.ins_urgency_medium.fg" },
    { "colors.ins_urgency_high.bg",   "colors.ins_urgency_high.fg"   }
  }
};

char *urgency_default_colors [N_STATES][N_UPAIRS][2] = {
  { // normal
    { "#52663DE8", "#999999" },
    { "#66523DE8", "#B3B3B3" },
    { "#663D3DE8", "#B3B3B3" }
  },

  { // alternate
    { "#66804DE8", "#B3B3B3" },
    { "#80664DE8", "#CCCCCC" },
    { "#734545E8", "#CCCCCC" }
  },

  { // insensitive
    { "#29331FE8", "#4D4D4D" },
    { "#33291FE8", "#4D4D4D" },
    { "#332929E8", "#4D4D4D" }
  }
};

// --

void loft_deinit (void) {
  ssize_t i;
  LoftObject *obj;
  // wrapper that doesn't interfere with global object list -- see object.c
  extern void _object_destroy (LoftObject *);

  debug(B_WHITE ">> " C_ESC RED "LOFT DEINIT\n" C_ESC);

  list_for_each(&loft.objects, i, obj) {
    _object_destroy(obj);
  }

  list_deinit(&loft.objects);
  list_deinit(&loft.redraws);
  map_deinit(&loft.config);

  if(loft.font.str != NULL)
    free(loft.font.str);

  pango_font_description_free(loft.font.desc);

  loft_break();

  XFlush(loft.display);
  XCloseDisplay(loft.display);
}

void loft_init_config (void) {
  bool free_config_base = false;
  char *config_base = getenv("XDG_CONFIG_HOME");

  if(config_base == NULL) {
    char *home_dir = getenv("HOME");
    if(home_dir == NULL) {
      fprintf(stderr, "$HOME is not set; you have bigger problems to worry about.\n");
      return;
    }

    free_config_base = true;
    config_base = malloc(strlen(home_dir) + strlen("/.config") + 1);
    if(config_base == NULL)
      return;

    sprintf(config_base, "%s/.config", home_dir);
  }

  char config_dir[strlen(config_base) + strlen("/loft") + 1];
  sprintf(config_dir, "%s/loft", config_base);

  if(free_config_base)
    free(config_base);

  char config_path[strlen(config_dir) + strlen("/config") + 1];
  sprintf(config_path, "%s/config", config_dir);

  struct stat st;
  if(stat(config_dir, &st) == -1) {
    mkdir(config_dir, S_IRUSR|S_IWUSR|S_IROTH);
    return;
  }

  FILE *fh = fopen(config_path, "r");
  if(fh == NULL)
    return;

  char *line;
  char **spl;
  size_t nspl;
  char *key;
  char *val;

  while(true) {
    if(freadline(fh, &line, NULL) != 0)
      break;

    spl = strsplit(line, "=", &nspl);
    free(line);

    if(spl == NULL)
      continue;

    if(nspl < 2) {
      strbuf_free(spl, nspl);
      continue;
    }

    key = strstrip(spl[0], NULL);
    val = strstrip(spl[1], NULL);

    map_insert(&loft.config, key, val, free);
    free(key);

    strbuf_free(spl, nspl);
  }

  fclose(fh);
}

void loft_init (void) {
  XInitThreads();
  setlocale(LC_ALL, "");

  loft.state = WITHDRAWN;
  loft.no_refresh = false;

  map_init(&loft.config, 16, 16);
  list_init(&loft.objects, 8, 8);
  list_init(&loft.redraws, 8, 8);

  loft.display = XOpenDisplay(NULL);
  if(loft.display == NULL) {
    fprintf(stderr, "loft: cannot open display.\n");
    exit(EXIT_FAILURE);
  }

  loft.display_fd = ConnectionNumber(loft.display);
  loft.screen = XDefaultScreen(loft.display);
  loft.root = XRootWindow(loft.display, loft.screen);

  loft.wm_delete_win = XInternAtom(loft.display, "WM_DELETE_WINDOW", false);
  loft.wm_state = XInternAtom(loft.display, "_NET_WM_STATE", false);
  loft.wm_state_fullscreen = XInternAtom(loft.display, "_NET_WM_STATE_FULLSCREEN", false);
  loft.wm_window_type = XInternAtom(loft.display, "_NET_WM_WINDOW_TYPE", false);
  loft.wm_window_type_dialog = XInternAtom(loft.display, "_NET_WM_WINDOW_TYPE_DIALOG", false);

  XVisualInfo vi;
  bool match = XMatchVisualInfo(loft.display, loft.screen, 32, TrueColor, &vi);

  if(match) {
    loft.depth = vi.depth;
    loft.visual = vi.visual;
    loft.colormap = XCreateColormap(loft.display, loft.root, loft.visual, AllocNone);
    loft.rgba = true;
  }
  else {
    loft.depth = DefaultDepth(loft.display, loft.screen);
    loft.visual = DefaultVisual(loft.display, loft.screen);
    loft.colormap = DefaultColormap(loft.display, loft.screen);
    loft.rgba = false;
  }

  loft.event_mask = ExposureMask | StructureNotifyMask |
    KeyPressMask | KeyReleaseMask |
    ButtonPressMask | ButtonReleaseMask |
    PointerMotionMask | EnterWindowMask | LeaveWindowMask |
    FocusChangeMask | VisibilityChangeMask;

  loft.font.name = NULL;
  loft.font.size = 0;
  loft.font.str = NULL;
  loft.font.desc = NULL;

  loft_init_config();
  loft_init_colors();
  loft_init_font(NULL, -1);

  XFlush(loft.display);
}

void loft_init_colors (void) {
  char *base = map_get(&loft.config, "colors.base");
  if(base == NULL)
    base = "#0D0D0DE8";

  rgba_from_str(&loft.colors.base, base);

  int s,p,c; // state, pair, color
  struct rgba *pair;
  char *hex;

  for(s = 0; s < N_STATES; ++s) {
    for(p = 0; p < N_PAIRS; ++p) {
      pair = (struct rgba*) &loft.colors.common[s][p];
      for(c = 0; c < 2; ++c) {
        hex = map_get(&loft.config, color_keys[s][p][c]);
        if(hex == NULL)
          hex = default_colors[s][p][c];

        rgba_from_str(&pair[c], hex);
      }
    }

    for(p = 0; p < N_UPAIRS; ++p) {
      pair = (struct rgba*) &loft.colors.urgency[s][p];
      for(c = 0; c < 2; ++c) {
        hex = map_get(&loft.config, urgency_color_keys[s][p][c]);
        if(hex == NULL)
          hex = urgency_default_colors[s][p][c];

        rgba_from_str(&pair[c], hex);
      }
    }
  }
}

void loft_init_font (char *name, int size) {
  if(name == NULL) {
    name = map_get(&loft.config, "font.name");
    if(name == NULL)
      name = "Monospace";
  }

  if(size <= 0) {
    char *size_str = map_get(&loft.config, "font.size");
    if(size_str != NULL)
      size = strtoul(size_str, NULL, 10);
    else
      size = 10;
  }

  loft.font.name = name;
  loft.font.size = size;

  if(loft.font.str != NULL)
    free(loft.font.str);

  if(loft.font.desc != NULL)
    pango_font_description_free(loft.font.desc);

  int font_str_len = strlen(loft.font.name) + 4;
  loft.font.str = malloc(font_str_len + 1);
  snprintf(loft.font.str, font_str_len, "%s %i", loft.font.name, loft.font.size);
  loft.font.desc = pango_font_description_from_string(loft.font.str);
}
