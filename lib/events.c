#include "loft.h"

#include <poll.h>
#include <cairo/cairo-xlib.h>
#include <X11/Xatom.h>
#include <X11/XKBlib.h>
#include <X11/Xutil.h>

extern void _arrange (LoftWidget *n);
extern void _refresh (LoftWidget *n); // -- refresh.c

void _propagate (LoftWidget *w, char *signal, void *arg) {
  if(w->visible == false || loft_emit(&w->obj, signal, arg) == 1)
    return;

  for(w = w->child; w != NULL; w = w->next)
    _propagate(w, signal, arg);
}

void _propagate_pointer (LoftWidget *w, LoftPointerEvent *pe) {
  if(w->visible == false)
    return;

  if(loft_widget_touching(w, &pe->p)) {
    if(w->prelight == false) {
      w->prelight = true;
      if(loft_emit(&w->obj, "pointer-enter", pe) == 1)
        return;
    }

    if(loft_emit(&w->obj, "pointer-motion", pe) == 1)
      return;
  }
  else if(w->prelight) {
    w->pressed = false;
    w->prelight = false;

    if(loft_emit(&w->obj, "pointer-leave", pe) == 1)
      return;
  }

  for(w = w->child; w != NULL; w = w->next)
    _propagate_pointer(w, pe);
}

void _propagate_press (LoftWidget *w, LoftButtonEvent *be, bool *propagate) {
  if(w->sensitive == false || w->visible == false)
    return;

  if(loft_widget_touching(w, &be->p)) {
    w->pressed = true;
    int ret = loft_emit(&w->obj, "button-press", be);

    if(ret == 1) {
      *propagate = false;
      return;
    }

    if(ret == 2) {
      *propagate = false;

      LoftWindow *win;
      loft_widget_native(w, (LoftWidget**) &win);
      LoftWidget *n = &win->w;

      XEvent ev;
      LoftPointerEvent pe = {
        be->state,
        .x = be->x,
        .y = be->y
      };

      w->dragging = true;
      loft_emit(&w->obj, "drag-start", &pe);

      while(true) {
        XMaskEvent(loft.display, loft.event_mask, &ev);

        // override MotionNotify events
        if(ev.type == MotionNotify) {
          pe.state = ev.xmotion.state;
          pe.x = ev.xmotion.x_root - n->x;
          pe.y = ev.xmotion.y_root - n->y;

          if(w->prelight == false && loft_widget_touching(w, &pe.p))
            w->prelight = true;
          else if(w->prelight)
            w->prelight = false;

          loft_emit(&w->obj, "drag-motion", &pe);
        }
        // re-propagate pointer position when released
        else if(ev.type == ButtonRelease && ev.xbutton.button == be->button) {
          w->pressed = false;
          loft_emit(&w->obj, "button-release", be);
          _propagate_pointer(n, &pe);

          break;
        }
        // break if window is unfocused/unmapped
        else if((ev.type == FocusOut || ev.type == UnmapNotify) && ev.xany.window == win->xwin) {
          w->pressed = false;
          loft_emit(&w->obj, "button-release", be);
          break;
        }
        // process expose/configure events
        else if(ev.type == Expose || ev.type == ConfigureNotify) {
          loft_process(&ev);
        }
      }

      w->dragging = false;
      loft_emit(&w->obj, "drag-release", &pe);

      return;
    }
  }

  for(w = w->child; w != NULL; w = w->next)
    _propagate_press(w, be, propagate);
}

void _propagate_release (LoftWidget *w, LoftButtonEvent *be) {
  if(w->sensitive == false || w->visible == false)
    return;

  if(w->pressed) {
    w->pressed = false;
    loft_emit(&w->obj, "button-release", be);
  }

  for(w = w->child; w != NULL; w = w->next)
    _propagate_release(w, be);
}

inline static
void _scan_event (int win, int type, XEvent *e) {
  while(XCheckTypedWindowEvent(loft.display, win, type, e));
}

// --

void loft_main (void (*pre)(XEvent *)) {
  XEvent ev;
  size_t i;
  LoftWidget *n;
  LoftWindow *win;

  loft.state = RUNNING;

  while(loft.state == RUNNING) {
    loft_lock();

    if(loft.redraws.used_size == 0 || XPending(loft.display)) {
      XNextEvent(loft.display, &ev);

      if(pre != NULL)
        pre(&ev);

      loft_process(&ev);
    }
    else {
      list_for_each(&loft.redraws, i,n) {
        if(n->dirty) {
          win = (LoftWindow *) n;
          cairo_xlib_surface_set_size(win->target, n->width, n->height);
          loft_widget_grow_buffer(n, n->width, n->height);

          _arrange(n);
        }

        _refresh(n);
      }

      list_clear(&loft.redraws);
    }

    loft_unlock();
  }

  loft_deinit();
}

bool loft_process (XEvent *ev) {
  LoftWindow *win = loft_window_from_xwin(ev->xany.window);
  if(win == NULL)
    return false;

  LoftWidget *n = &win->w;

  if(ev->type == Expose) {
    XExposeEvent *e = &ev->xexpose;
    if(e->count == 0) {
      if(win->size_propagated == false) {
        struct wh s;
        int base_w,
            base_h;

        s.width = n->width;
        s.height = n->height;

        loft_emit(&n->obj, "size-request", &s);
        loft_widget_base_size(n, &base_w, &base_h);

        if(s.width < base_w || ((win->constraints & CONSTRAIN_W) && s.width > base_w))
          s.width = base_w;
        if(s.height < base_h || ((win->constraints & CONSTRAIN_H) && s.height > base_h))
          s.height = base_h;

        if(s.width != n->width || s.height != n->height) {
          loft_window_resize(win, s.width, s.height);
          win->size_propagated = true;
          return true;
        }
        else {
          win->size_propagated = true;
        }
      }

      n->stale = true;
      list_append(&loft.redraws, n, NULL);
    }
  }
  else if(ev->type == ConfigureNotify) {
    _scan_event(win->xwin, ConfigureNotify, ev);
    XConfigureEvent *c = &ev->xconfigure;

    if(c->x != n->x || c->y != n->y) {
      n->old_x = n->x;
      n->old_y = n->y;
      n->x = c->x;
      n->y = c->y;

      loft_emit(&n->obj, "move", NULL);
    }

    if(c->width != n->width || c->height != n->height) {
      n->old_w = n->width;
      n->old_h = n->height;
      n->width = c->width;
      n->height = c->height;

      n->dirty = true;
      win->size_propagated = false;

      loft_emit(&n->obj, "resize", NULL);
    }

    loft_emit(&n->obj, "configure", NULL);
  }
  else if(ev->type == KeyPress) {
    XKeyEvent *ke = &ev->xkey;
    LoftKey k = {
      .state = ke->state
    };

    k.printable = XLookupString(ke, k.str, 6, &k.sym, NULL) > 0;
    _propagate(n, "key-press", &k);
  }
  else if(ev->type == KeyRelease) {
    XKeyEvent *ke = &ev->xkey;
    LoftKey k = {
      .state = ke->state
    };

    k.printable = XLookupString(ke, k.str, 6, &k.sym, NULL) > 0;
    _propagate(n, "key-release", &k);
  }
  else if(ev->type == MotionNotify) {
    _scan_event(win->xwin, MotionNotify, ev);

    XMotionEvent *m = &ev->xmotion;
    LoftPointerEvent pe = {
      m->state,
      .x = m->x,
      .y = m->y
    };

    loft_emit(&n->obj, "pointer-motion", &pe);

    if(n->child != NULL)
      _propagate_pointer(n->child, &pe);
  }
  else if(ev->type == EnterNotify) {
    LoftPointerEvent pe = {
      ev->xcrossing.state,
      .x = ev->xcrossing.x,
      .y = ev->xcrossing.y
    };

    loft_emit(&n->obj, "pointer-enter", &pe);

    if(n->child != NULL)
      _propagate_pointer(n->child, &pe);
  }
  else if(ev->type == LeaveNotify) {
    LoftPointerEvent pe = {
      ev->xcrossing.state,
      .x = ev->xcrossing.x,
      .y = ev->xcrossing.y
    };

    n->pressed = false;
    loft_emit(&n->obj, "pointer-leave", &pe);

    if(n->child != NULL)
      _propagate_pointer(n->child, &pe);
  }
  else if(ev->type == ButtonPress) {
    LoftButtonEvent be = {
      ev->xbutton.button,
      ev->xbutton.state,
      .x = ev->xbutton.x,
      .y = ev->xbutton.y
    };

    n->pressed = true;

    if(loft_emit(&n->obj, "button-press", &be) == 1)
      return true;

    if(n->child != NULL) {
      bool propagate = true;
      _propagate_press(n->child, &be, &propagate);
    }
  }
  else if(ev->type == ButtonRelease) {
    LoftButtonEvent be = {
      ev->xbutton.button,
      ev->xbutton.state,
      .x = ev->xbutton.x,
      .y = ev->xbutton.y
    };

    n->pressed = false;

    if(loft_emit(&n->obj, "button-release", &be) == 1)
      return true;

    if(n->child != NULL)
      _propagate_release(n->child, &be);
  }
  else if(ev->type == FocusIn) {
    n->has_focus = true;
    n->has_input_focus = true;

    if(win->focused != NULL) {
      win->focused->has_input_focus = true;
      loft_emit(&win->focused->obj, "set-input-focus", &win->focused->has_input_focus);
    }
  }
  else if(ev->type == FocusOut) {
    n->has_focus = true;
    n->has_input_focus = false;

    if(win->focused != NULL) {
      win->focused->has_input_focus = false;
      loft_emit(&win->focused->obj, "set-input-focus", &win->focused->has_input_focus);
    }
  }
  else if(ev->type == MapNotify) {
    n->visible = true;
    loft_emit(&n->obj, "show", NULL);
    loft_emit(&n->obj, "set-visible", &n->visible);
  }
  else if(ev->type == UnmapNotify) {
    n->pressed = false;
    n->visible = false;
    loft_emit(&n->obj, "hide", NULL);
    loft_emit(&n->obj, "set-visible", &n->visible);
  }
  else if(ev->type == ClientMessage) {
    XClientMessageEvent *c = &ev->xclient;
    if(c->data.s[0] == loft.wm_delete_win && c->data.l[0] == loft.wm_delete_win)
      loft_emit(&n->obj, "delete", NULL);
  }
  else {
    return false;
  }

  return true;
}
