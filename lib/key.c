#include "loft.h"
#include <clip/string.h>
#include <clip/utf8.h>

bool loft_str_to_key (char *str, LoftKey *k) {
  size_t nspl;
  char **spl = strsplit(str, "-", &nspl);
  if(spl == NULL)
    return false;

  char *state_str;
  char *key_str;

  if(nspl > 1) {
    state_str = spl[0];
    key_str = spl[1];
  }
  else {
    state_str = NULL;
    key_str = spl[0];
  }

  if(utf8_strlen(key_str, NULL) > 1) {
    strbuf_free(spl, nspl);
    return false;
  }

  k->state = 0;

  if(state_str != NULL) {
    int i;
    int len = strlen(state_str);

    for(i = 0; i < len; i++) {
      if(state_str[i] == 'A')
        k->state |= Mod1Mask;
      else if(state_str[i] == 'M')
        k->state |= Mod4Mask;
      else if(state_str[i] == 'C')
        k->state |= ControlMask;
      else if(state_str[i] == 'S')
        k->state |= ShiftMask;
    }
  }

  k->sym = XStringToKeysym(key_str);
  snprintf(k->str, 6, "%s", key_str);
  k->printable = strlen(key_str) > 0;

  strbuf_free(spl, nspl);
  return true;
}
