#include "loft.h"

char *LABEL = "loft-label";

void _label_update_layout (LoftLabel *lb) {
  if(lb->str == NULL)
    return;

  if(lb->use_markup)
    pango_layout_set_markup(lb->_lt, lb->str, -1);
  else
    pango_layout_set_text(lb->_lt, lb->str, -1);
}

// --

int _label_on_deinit (LoftLabel *lb, void *arg, void *data) {
  loft_timer_stop(&lb->_pulse_timer);
  g_object_unref(lb->_lt);
  return 0;
}

int _label_on_draw (LoftLabel *lb, cairo_t *cr, void *data) {
  LoftWidget *w = &lb->w;

  int state;
  if(w->sensitive == false)
    state = INSENSITIVE;
  else if(lb->highlighted)
    state = ACTIVE;
  else
    state = NORMAL;

  int pair;
  if(lb->highlighted)
    pair = SELECTED;
  else
    pair = IDLE;

  int sl = lb->bg_slant;
  if(sl < 0)
    sl *= -1;

  if(lb->draw_bg || lb->highlighted) {
    rgba_cairo_set(cr, &w->colors[state][pair].bg);

    if(lb->bg_slant == 0) {
      cairo_rectangle(cr, 0, 0, w->width, w->height);
    }
    else {
      if(lb->bg_slant < 0) {
        cairo_move_to(cr, 0, 0);
        cairo_line_to(cr, sl, w->height);
        cairo_line_to(cr, w->width, w->height);
        cairo_line_to(cr, w->width - sl, 0);
      }
      else {
        cairo_move_to(cr, sl, 0);
        cairo_line_to(cr, 0, w->height);
        cairo_line_to(cr, w->width - sl, w->height);
        cairo_line_to(cr, w->width, 0);
      }

      cairo_close_path(cr);
    }

    cairo_fill(cr);
  }

  if(lb->str != NULL) {
    int text_x = lb->padding.left;
    if(lb->draw_bg)
      text_x += sl;

    int th;
    pango_layout_get_pixel_size(lb->_lt, NULL, &th);

    rgba_cairo_set(cr, &w->colors[state][pair].fg);
    cairo_move_to(cr, text_x, (w->height - th) / 2);

    pango_cairo_update_layout(cr, lb->_lt);
    pango_cairo_show_layout(cr, lb->_lt);
  }

  return 0;
}

int _label_on_size_request (LoftLabel *lb, struct wh *size_req, void *data) {
  int hp = lb->padding.left + lb->padding.right;
  int vp = lb->padding.top + lb->padding.bottom;

  int min_w = hp,
      min_h = vp;

  if(lb->draw_bg && lb->bg_slant != 0)
    min_w += lb->bg_slant * (lb->bg_slant > 0 ? 2 : -2);

  if(lb->str != NULL) {
    double width;
    if(size_req->width <= 0)
      width = size_req->width;
    else if(size_req->width > min_w)
      width = (size_req->width - min_w) * PANGO_SCALE;
    else
      width = 0;

    pango_layout_set_width(lb->_lt, width);

    int tw,th;
    pango_layout_get_pixel_size(lb->_lt, &tw, &th);

    if(lb->ellipsize == ELLIPSIZE_NONE)
      min_w += tw;

    min_h += th;
  }

  loft_widget_set_min_size(&lb->w, min_w, min_h);
  return 0;
}

int _label_on_pulse_timeout (LoftTimer *t, LoftLabel *lb) {
  lb->_draw_pulse ^= true;
  loft_widget_refresh_fast(&lb->w);
  return 0;
}

// --

int loft_label_get_bg_slant (LoftLabel *lb) {
  return lb->bg_slant;
}

int loft_label_get_ellipsize (LoftLabel *lb) {
  return lb->ellipsize;
}

bool loft_label_get_word_wrap (LoftLabel *lb) {
  return lb->word_wrap;
}

char *loft_label_get_str (LoftLabel *lb) {
  return lb->str;
}

bool loft_label_get_use_markup (LoftLabel *lb) {
  return lb->use_markup;
}

void loft_label_init (LoftLabel *lb, char *str, bool use_markup) {
  loft_widget_init(&lb->w, LABEL, true, true, SIZE_STATIC);

  loft_connect(&lb->w.obj, "deinit", _label_on_deinit, NULL);
  loft_connect(&lb->w.obj, "draw", _label_on_draw, NULL);
  loft_connect(&lb->w.obj, "size-request", _label_on_size_request, NULL);

  lb->alignment = ALIGN_LEFT;
  lb->bg_slant = 0;
  lb->draw_bg = false;
  lb->ellipsize = ELLIPSIZE_NONE;
  lb->highlighted = false;
  lb->pulse = false;
  lb->str = str;
  lb->use_markup = use_markup;
  lb->word_wrap = false;

  lb->padding.left = 2;
  lb->padding.top = 2;
  lb->padding.right = 2;
  lb->padding.bottom = 2;

  lb->pulse_fade[0] = 5;
  lb->pulse_fade[1] = 10;
  lb->pulse_timeout = 500;

  cairo_t *cr = cairo_create(lb->w.buffer);
  lb->_lt = pango_cairo_create_layout(cr);
  cairo_destroy(cr);

  pango_layout_set_auto_dir(lb->_lt, true);
  pango_layout_set_font_description(lb->_lt, loft.font.desc);

  _label_update_layout(lb);

  lb->_draw_pulse = false;
  loft_timer_init(&lb->_pulse_timer, lb->pulse_timeout, _label_on_pulse_timeout, lb);
}

bool loft_label_is_highlighted (LoftLabel *lb) {
  return lb->highlighted;
}

LoftLabel *loft_label_new (char *str, bool use_markup) {
  LoftLabel *lb = malloc(sizeof(LoftLabel));
  if(lb == NULL)
    return NULL;

  loft_label_init(lb, str, use_markup);
  loft_object_set_freeable(&lb->w.obj, true);

  return lb;
}

void loft_label_set_alignment (LoftLabel *lb, int alignment) {
  lb->alignment = alignment;
  pango_layout_set_alignment(lb->_lt, lb->alignment);
  loft_widget_refresh(&lb->w, false);
}

void loft_label_set_bg_slant (LoftLabel *lb, int bg_slant) {
  lb->bg_slant = bg_slant;
  loft_widget_refresh(&lb->w, lb->draw_bg);
}

void loft_label_set_draw_bg (LoftLabel *lb, bool draw_bg) {
  lb->draw_bg = draw_bg;
  loft_widget_refresh(&lb->w, lb->bg_slant != 0);
}

void loft_label_set_ellipsize (LoftLabel *lb, int ellipsize) {
  lb->ellipsize = ellipsize;
  pango_layout_set_ellipsize(lb->_lt, lb->ellipsize);
  loft_widget_refresh(&lb->w, true);
}

void loft_label_set_highlighted (LoftLabel *lb, bool highlighted) {
  lb->highlighted = highlighted;
  loft_widget_refresh(&lb->w, false);
}

void loft_label_set_padding (LoftLabel *lb, int l, int t, int r, int b) {
  if(l >= 0)
    lb->padding.left = l;
  if(t >= 0)
    lb->padding.top = t;
  if(r >= 0)
    lb->padding.right = r;
  if(b >= 0)
    lb->padding.bottom = b;

  loft_widget_refresh(&lb->w, true);
}

void loft_label_set_pulse (LoftLabel *lb, bool pulse) {
  lb->pulse = pulse;
  loft_emit(&lb->w.obj, "set-pulse", &lb->pulse);

  if(lb->pulse) {
    loft_timer_start(&lb->_pulse_timer);
  }
  else {
    loft_timer_stop(&lb->_pulse_timer);
    if(lb->_draw_pulse) {
      lb->_draw_pulse = false;
      loft_widget_refresh_fast(&lb->w);
    }
  }
}

void loft_label_set_pulse_fade (LoftLabel *lb, double value, double max) {
  lb->pulse_fade[0] = value;
  lb->pulse_fade[1] = max;
}

void loft_label_set_pulse_timeout (LoftLabel *lb, int pulse_timeout) {
  lb->pulse_timeout = pulse_timeout;
  loft_timer_set_timeout(&lb->_pulse_timer, lb->pulse_timeout);
}

void loft_label_set_str (LoftLabel *lb, char *str, bool use_markup) {
  lb->str = str;
  lb->use_markup = use_markup;
  _label_update_layout(lb);
  loft_widget_refresh(&lb->w, true);
}

void loft_label_set_word_wrap (LoftLabel *lb, bool word_wrap) {
  if(word_wrap == lb->word_wrap)
    return;

  lb->word_wrap = word_wrap;
  lb->w.size_mode = lb->word_wrap ? SIZE_H4W : SIZE_STATIC;
  pango_layout_set_wrap(lb->_lt, PANGO_WRAP_WORD);
  loft_widget_refresh(&lb->w, true);
}
