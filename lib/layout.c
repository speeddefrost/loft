#include "loft.h"

char *LAYOUT = "loft-layout";

int _layout_arrange_h (LoftLayout *lt, void *arg, void *data) {
  LoftWidget *w = &lt->w;
  if(w->child == NULL)
    return 0;

  LoftWidget *c;
  int base_w, base_h;

  int needed_w = 0;
  int n_expanded = 0;
  int n_visible = 0;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    loft_widget_base_size(c, &base_w, NULL);
    needed_w += base_w;

    if(c->attrs & EXPAND_X)
      ++n_expanded;

    ++n_visible;
  }

  if(lt->spacing > 0 && n_visible > 1)
    needed_w += lt->spacing * (n_visible - 1);

  int spare_w;
  if(n_expanded > 0)
    spare_w = (w->width - needed_w) / n_expanded;
  else
    spare_w = 0;

  int fill_w;
  int fill_h = w->height;

  int x = w->x;
  int y = w->y;

  int adv_x;
  int cx,cy,cw,ch;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    loft_widget_base_size(c, &base_w, &base_h);

    if(c->attrs & EXPAND_X) {
      fill_w = base_w + spare_w;
      adv_x = fill_w;

      if(c->attrs & PACK_X) {
        cw = base_w;

        if(c->attrs & FLOW_L)
          cx = x;
        else if(c->attrs & FLOW_R)
          cx = x + (fill_w - base_w);
        else
          cx = x + ((fill_w - base_w) / 2);
      }
      else {
        cw = fill_w;
        cx = x;
      }
    }
    else {
      cw = base_w;
      cx = x;
      adv_x = base_w;
    }

    if(c->attrs & EXPAND_Y) {
      if(c->attrs & PACK_Y) {
        ch = base_h;

        if(c->attrs & FLOW_U)
          cy = y;
        else if(c->attrs & FLOW_D)
          cy = y + (fill_h - base_h);
        else
          cy = y + ((fill_h - base_h) / 2);
      }
      else {
        ch = fill_h;
        cy = y;
      }
    }
    else {
      ch = base_h;

      if(c->attrs & FLOW_U)
        cy = y;
      else if(c->attrs & FLOW_D)
        cy = y + (fill_h - base_h);
      else
        cy = y + ((fill_h - base_h) / 2);
    }

    loft_widget_configure(c, cx, cy, cw, ch);
    x += adv_x + lt->spacing;
  }

  return 0;
}

int _layout_arrange_v (LoftLayout *lt, void *arg, void *data) {
  LoftWidget *w = &lt->w;
  if(w->child == NULL)
    return 0;

  LoftWidget *c;
  int base_w, base_h;

  int needed_h = 0;
  int n_expanded = 0;
  int n_visible = 0;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    loft_widget_base_size(c, NULL, &base_h);
    needed_h += base_h;

    if(c->attrs & EXPAND_Y)
      ++n_expanded;

    ++n_visible;
  }

  if(lt->spacing > 0 && n_visible > 1)
    needed_h += lt->spacing * (n_visible - 1);

  int spare_h;
  if(n_expanded > 0)
    spare_h = (w->height - needed_h) / n_expanded;
  else
    spare_h = 0;

  int fill_w = w->width;
  int fill_h;

  int x = w->x;
  int y = w->y;

  int adv_y;
  int cx,cy,cw,ch;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    loft_widget_base_size(c, &base_w, &base_h);

    if(c->attrs & EXPAND_X) {
      if(c->attrs & PACK_X) {
        cw = base_w;

        if(c->attrs & FLOW_L)
          cx = x;
        else if(c->attrs & FLOW_R)
          cx = x + (fill_w - base_w);
        else
          cx = x + ((fill_w - base_w) / 2);
      }
      else {
        cw = fill_w;
        cx = x;
      }
    }
    else {
      cw = base_w;

      if(c->attrs & FLOW_L)
        cx = x;
      else if(c->attrs & FLOW_R)
        cx = x + (fill_w - base_w);
      else
        cx = x + ((fill_w - base_w) / 2);
    }

    if(c->attrs & EXPAND_Y) {
      fill_h = base_h + spare_h;
      adv_y = fill_h;

      if(c->attrs & PACK_Y) {
        ch = base_h;

        if(c->attrs & FLOW_U)
          cy = y;
        else if(c->attrs & FLOW_D)
          cy = y + (fill_h - base_h);
        else
          cy = y + ((fill_h - base_h) / 2);
      }
      else {
        ch = fill_h;
        cy = y;
      }
    }
    else {
      adv_y = base_h;
      ch = base_h;
      cy = y;
    }

    loft_widget_configure(c, cx, cy, cw, ch);
    y += adv_y + lt->spacing;
  }

  return 0;
}

int _layout_size_request_v (LoftLayout *lt, struct wh *size_req, void *data) {
  LoftWidget *w = &lt->w;
  int min_w = 0,
      min_h = 0;

  if(w->child == NULL)
    goto VLT_SET_MIN;

  LoftWidget *c;
  struct wh c_size_req = { .height = -1 };
  int base_w,base_h;

  int n_visible = 0;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    ++n_visible;

    c_size_req.width = c->size_mode == SIZE_H4W ? 0 : -1;
    loft_emit(&c->obj, "size-request", &c_size_req);
    loft_widget_base_size(c, &base_w, NULL);

    if(base_w > min_w)
      min_w = base_w;
  }

  if(size_req->width < min_w)
    size_req->width = min_w;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    c_size_req = (struct wh) { size_req->width, -1 };
    loft_emit(&c->obj, "size-request", &c_size_req);

    loft_widget_base_size(c, NULL, &base_h);
    min_h += base_h;
  }

  if(lt->spacing > 0 && n_visible > 1)
    min_h += lt->spacing * (n_visible - 1);

VLT_SET_MIN:

  loft_widget_set_min_size(w, min_w, min_h);
  return 0;
}

int _layout_size_request_h (LoftLayout *lt, struct wh *size_req, void *data) {
  LoftWidget *w = &lt->w;
  int min_w = 0,
      min_h = 0;

  if(w->child == NULL)
    goto HLT_SET_MIN;

  LoftWidget *c;
  struct wh c_size_req = { .height = -1 };
  int base_w, base_h;

  int n_visible = 0,
      n_expanded = 0;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    if(c->size_mode == SIZE_H4W)
      c_size_req.width = 0;
    else // c->size_mode == SIZE_STATIC
      c_size_req.width = -1;

    ++n_visible;
    if(c->attrs & EXPAND_X)
      ++n_expanded;

    loft_emit(&c->obj, "size-request", &c_size_req);
    loft_widget_base_size(c, &base_w, NULL);

    min_w += base_w;
  }

  if(lt->spacing > 0 && n_visible > 1)
    min_w += lt->spacing * (n_visible - 1);

  double spare_w = size_req->width - min_w;
  if(n_expanded > 1)
    spare_w /= n_expanded;

  for(c = w->child; c != NULL; c = c->next) {
    if(c->visible == false)
      continue;

    loft_widget_base_size(c, &base_w, NULL);
    c_size_req.width = base_w + spare_w;
    loft_emit(&c->obj, "size-request", &c_size_req);

    loft_widget_base_size(c, NULL, &base_h);
    if(base_h > min_h)
      min_h = base_h;
  }

HLT_SET_MIN:

  loft_widget_set_min_size(w, min_w, min_h);
  return 0;
}

inline static void  _layout_reconfigure (LoftLayout *lt) {
  if(lt->aspect == ASPECT_V) {
    loft_sh_set_callback(lt->_arrange, _layout_arrange_v);
    loft_sh_set_callback(lt->_size_request, _layout_size_request_v);
  }
  else { // lt->aspect == ASPECT_H
    loft_sh_set_callback(lt->_arrange, _layout_arrange_h);
    loft_sh_set_callback(lt->_size_request, _layout_size_request_h);
  }
}

void _layout_init (LoftLayout *lt, char *type, bool drawable, int aspect, int spacing) {
  loft_widget_init(&lt->w, type, true, drawable, SIZE_H4W);

  lt->aspect = aspect;
  lt->spacing = spacing;

  lt->_arrange = loft_connect(&lt->w.obj, "arrange", NULL, NULL);
  lt->_size_request = loft_connect(&lt->w.obj, "size-request", NULL, NULL);

  _layout_reconfigure(lt);
}

// --

void loft_layout_init (LoftLayout *lt, int aspect, int spacing) {
  _layout_init(lt, LAYOUT, false, aspect, spacing);
}

void loft_layout_init_with_type (LoftLayout *lt, char *type, bool drawable, int aspect, int spacing) {
  _layout_init(lt, type, drawable, aspect, spacing);
}

LoftLayout *loft_layout_new (int aspect, int spacing) {
  LoftLayout *lt = malloc(sizeof(LoftLayout));
  if(lt == NULL)
    return NULL;

  loft_layout_init(lt, aspect, spacing);
  loft_object_set_freeable(&lt->w.obj, true);

  return lt;
}

LoftLayout *loft_layout_new_with_type (char *type, bool drawable, int aspect, int spacing) {
  LoftLayout *lt = malloc(sizeof(LoftLayout));
  if(lt == NULL)
    return NULL;

  loft_layout_init_with_type(lt, type, drawable, aspect, spacing);
  loft_object_set_freeable(&lt->w.obj, true);

  return lt;
}

void loft_layout_set_aspect (LoftLayout *lt, int aspect) {
  lt->aspect = aspect;
  _layout_reconfigure(lt);
  loft_queue_refresh(&lt->w, true);
}

void loft_layout_set_spacing (LoftLayout *lt, int spacing) {
  lt->spacing = spacing;
  loft_queue_refresh(&lt->w, true);
}
