#include "loft.h"

void loft_cairo_solid_rectangle (cairo_t *cr, int x, int y, int w, int h) {
  double xl = x + 0.5;
  double xr = x + w - 0.5;

  double yt = y + 0.5;
  double yb = y + h - 0.5;

  cairo_move_to(cr, xl, yt);
  cairo_line_to(cr, xr, yt);
  cairo_line_to(cr, xr, yb);
  cairo_line_to(cr, xl, yb);
  cairo_line_to(cr, xl, y);
}

// determine if rectangle intersects an area
bool loft_xywh_intersecting (struct xywh *r, struct xywh *a) {
  return r->x < (a->x + a->width) && (r->x + r->width) > a->x &&
         r->y < (a->y + a->height) && (r->y + r->height) > a->y;
}

// determine if point is within an area
bool loft_xy_within (struct xy *p, struct xywh *a) {
  return p->x >= a->x && p->x < (a->x + a->width) &&
         p->y >= a->y && p->y < (a->y + a->height);
}

// clip rectangle to area
void loft_xywh_clip (struct xywh *r, struct xywh *a) {
  if(r->x < a->x) {
    r->x = a->x;

    int xdiff = a->x - r->x;
    if(xdiff > r->width)
      r->width -= xdiff;
    else
      r->width = 0;
  }

  if(r->y < a->y) {
    r->y = a->y;

    int ydiff = a->y - r->y;
    if(ydiff > r->height)
      r->height -= ydiff;
    else
      r->height = 0;
  }

  int rxr = r->x + r->width;
  int cxr = a->x + a->width;

  if(rxr > cxr) {
    int xrdiff = rxr - cxr;
    if(xrdiff > r->width)
      r->width -= xrdiff;
    else
      r->width = 0;
  }

  int ryb = r->y + r->height;
  int cyb = a->y + a->height;

  if(ryb > cyb) {
    int ybdiff = ryb - cyb;
    if(r->height > ybdiff)
      r->height -= ybdiff;
    else
      r->height = 0;
  }
}
