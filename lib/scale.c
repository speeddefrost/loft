#include "loft.h"
#include <clip/range.h>

char *SCALE = "loft-scale";

void _scale_adjust_min_size (LoftScale *sc) {
  if(sc->aspect == ASPECT_H) {
    sc->w.min_w = ABS_MIN_SIZE * 4;
    sc->w.min_h = ABS_MIN_SIZE;
  }
  else { // sc->aspect == ASPECT_V
    sc->w.min_w = ABS_MIN_SIZE;
    sc->w.min_h = ABS_MIN_SIZE * 4;
  }
}

void _scale_adjust_pref_size (LoftScale *sc) {
  LoftWidget *w = &sc->w;

  if(sc->aspect == ASPECT_H) {
    if(w->pref_h > w->pref_w / 4)
      w->pref_h = w->pref_w / 4;
  }
  else { // sc->aspect == ASPECT_V
    if(w->pref_w < w->pref_h / 4)
      w->pref_w = w->pref_h / 4;
  }
}

void _scale_invert_pref_size (LoftScale *sc) {
  sc->w.pref_w ^= sc->w.pref_h;
  sc->w.pref_h ^= sc->w.pref_w;
  sc->w.pref_w ^= sc->w.pref_h;
}

void _scale_update_span (LoftScale *sc) {
  if(sc->aspect == ASPECT_H) {
    sc->span.min = sc->w.height / 2;
    sc->span.max = sc->w.width - sc->span.min;
  }
  else { // sc->aspect == ASPECT_V
    sc->span.min = sc->w.width / 2;
    sc->span.max = sc->w.height - sc->span.min;
  }
}

void _scale_update_pos (LoftScale *sc) {
  if(sc->adj != NULL) {
    double perc;

    if(sc->aspect == ASPECT_H)
      perc = loft_adj_get_perc_bounded(sc->adj);
    else // sc->aspect == ASPECT_V
      perc = 100 - loft_adj_get_perc_bounded(sc->adj);

    sc->pos = perc_to_range(perc, sc->span.min, sc->span.max);
  }
  else {
    sc->pos = sc->span.min;
  }

  if(sc->inverted)
    sc->pos = (sc->span.max - sc->pos) + sc->span.min;
}

void _scale_update_sl_geo (LoftScale *sc) {
  if(sc->aspect == ASPECT_H) {
    sc->sl_geo = (struct xywh) {
      sc->w.x + (sc->pos - (sc->w.height / 2)), sc->w.y,
        sc->w.height,
        sc->w.height
    };
  }
  else { // sc->aspect == ASPECT_V
    sc->sl_geo = (struct xywh) {
      sc->w.x, sc->w.y + (sc->pos - (sc->w.width / 2)),
        sc->w.width,
        sc->w.width
    };
  }
}

void _scale_update_sl_prelight (LoftScale *sc, struct xy *p) {
  if(loft_xy_within(p, &sc->sl_geo)) {
    sc->sl_prelight = true;
    loft_widget_refresh_fast(&sc->w);
  }
  else if(sc->sl_prelight) {
    sc->sl_prelight = false;
    loft_widget_refresh_fast(&sc->w);
  }
}

// --

int _scale_adj_on_changed (LoftAdjustment *adj, void *arg, LoftScale *sc) {
  _scale_update_pos(sc);
  _scale_update_sl_geo(sc);
  _scale_update_sl_prelight(sc, &sc->pointer);
  loft_widget_refresh_fast(&sc->w);
  return 0;
}

int _scale_on_button_press (LoftScale *sc, LoftButtonEvent *be, void *data) {
  if(be->button == 1) {
    unsigned int val;
    if(sc->aspect == ASPECT_H) {
      if(loft_xy_within(&be->p, &sc->sl_geo)) {
        sc->drag_offset = be->x - (sc->sl_geo.x + (sc->sl_geo.width / 2));
        return 2;
      }

      val = be->x - sc->w.x;
      if(sc->inverted)
        val = (sc->span.max - val) + sc->span.min;
    }
    else { // sc->aspect == ASPECT_V
      if(loft_xy_within(&be->p, &sc->sl_geo)) {
        sc->drag_offset = be->y - (sc->sl_geo.y + (sc->sl_geo.height / 2));
        return 2;
      }

      val = be->y - sc->w.y;
      if(sc->inverted == false)
        val = (sc->span.max - val) + sc->span.min;
    }

    loft_adj_set_perc(sc->adj, range_to_perc(val, sc->span.min, sc->span.max));
  }
  else {
    int up, down;

    if(sc->aspect == ASPECT_H) {
      if(sc->inverted) {
        up = 4;
        down = 5;
      }
      else {
        up = 5;
        down = 4;
      }
    }
    else { // sc->aspect == ASPECT_V
      if(sc->inverted) {
        up = 5;
        down = 4;
      }
      else {
        up = 4;
        down = 5;
      }
    }

    if(be->button == up)
      loft_adj_set_value_bounded(sc->adj, sc->adj->value + sc->step);
    else if(be->button == down)
      loft_adj_set_value_bounded(sc->adj, sc->adj->value - sc->step);

    _scale_update_sl_prelight(sc, &be->p);
  }

  return 0;
}

int _scale_on_drag_motion (LoftScale *sc, LoftPointerEvent *pe, void *data) {
  sc->pointer = pe->p;

  int val;
  if(sc->aspect == ASPECT_H) {
    val = pe->x - sc->w.x;
    if(sc->inverted)
      val = (sc->span.max - val) + sc->span.min;
  }
  else { // sc->aspect == ASPECT_V
    val = pe->y - sc->w.y;
    if(sc->inverted == false)
      val = (sc->span.max - val) + sc->span.min;
  }

  loft_adj_set_perc(sc->adj, range_to_perc(val, sc->span.min, sc->span.max));
  return 0;
}

int _scale_on_drag_release (LoftScale *sc, LoftPointerEvent *pe, void *data) {
  if(sc->w.prelight == false) {
    sc->sl_prelight = false;
    loft_widget_refresh_fast(&sc->w);
  }

  return 0;
}

int _scale_on_draw (LoftScale *sc, cairo_t *cr, void *data) {
  int state;
  if(sc->w.sensitive)
    state = NORMAL;
  else
    state = INSENSITIVE;

  struct rgba *bg1;
  struct rgba *bg2;
  struct rgba *bg3;
  struct rgba *bg4;

  if(sc->inverted) {
    bg1 = &sc->w.colors[INSENSITIVE][IDLE].bg;
    bg2 = &sc->w.colors[state][IDLE].bg;
    bg3 = &sc->w.colors[state][SELECTED].bg;
    bg4 = &sc->w.colors[INSENSITIVE][SELECTED].bg;
  }
  else {
    bg1 = &sc->w.colors[INSENSITIVE][SELECTED].bg;
    bg2 = &sc->w.colors[state][SELECTED].bg;
    bg3 = &sc->w.colors[state][IDLE].bg;
    bg4 = &sc->w.colors[INSENSITIVE][IDLE].bg;
  }

  struct rgba *fg;

  if(sc->sl_prelight || sc->w.dragging)
    fg = &sc->w.colors[state][PRELIGHT].fg;
  else
    fg = &sc->w.colors[state][IDLE].fg;

  if(sc->aspect == ASPECT_H) {
    double slider_r = sc->w.height / 2;
    double fill_h = sc->w.height / sc->fill_div;
    double y_pos = (sc->w.height - fill_h) / 2;

    rgba_cairo_set(cr, bg1);
    cairo_rectangle(cr, 0, y_pos, sc->span.min, fill_h);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg2);
    cairo_rectangle(cr, sc->span.min, y_pos, sc->pos, fill_h);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg3);
    cairo_rectangle(cr, sc->pos, y_pos, sc->span.max - sc->pos, fill_h);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg4);
    cairo_rectangle(cr, sc->span.max, y_pos, sc->w.width, fill_h);
    cairo_fill(cr);

    rgba_cairo_set(cr, fg);
    cairo_arc(cr, sc->pos, sc->w.height / 2, slider_r, 0.0, 360.0);
    cairo_fill(cr);
  }
  else { // sc->aspect == ASPECT_V
    double slider_r = sc->w.width / 2;
    double fill_w = sc->w.width / sc->fill_div;
    double x_pos = (sc->w.width - fill_w) / 2;

    rgba_cairo_set(cr, bg1);
    cairo_rectangle(cr, x_pos, sc->span.max, fill_w, sc->w.height);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg2);
    cairo_rectangle(cr, x_pos, sc->pos, fill_w, sc->span.max - sc->pos);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg3);
    cairo_rectangle(cr, x_pos, sc->span.min, fill_w, sc->pos);
    cairo_fill(cr);

    rgba_cairo_set(cr, bg4);
    cairo_rectangle(cr, x_pos, 0, fill_w, sc->span.min);
    cairo_fill(cr);

    rgba_cairo_set(cr, fg);
    cairo_arc(cr, sc->w.width / 2, sc->pos, slider_r, 0.0, 360.0);
    cairo_fill(cr);
  }

  return 0;
}

int _scale_on_pointer_leave (LoftScale *sc, void *arg, void *data) {
  sc->sl_prelight = false;
  loft_widget_refresh_fast(&sc->w);
  return 0;
}

int _scale_on_pointer_motion (LoftScale *sc, LoftPointerEvent *pe, void *data) {
  sc->pointer = pe->p;
  _scale_update_sl_prelight(sc, &pe->p);
  return 0;
}

int _scale_on_resize (LoftScale *sc, void *arg, void *data) {
  _scale_update_span(sc);
  _scale_update_pos(sc);
  _scale_update_sl_geo(sc);
  return 0;
}

int _scale_on_set_pref_size (LoftScale *sc, void *arg, void *data) {
  _scale_adjust_pref_size(sc);
  return 0;
}

// --

int loft_scale_get_aspect (LoftScale *sc) {
  return sc->aspect;
}

bool loft_scale_has_adj (LoftScale *sc) {
  return sc->adj != NULL;
}

void loft_scale_init (LoftScale *sc, int aspect, LoftAdjustment *adj) {
  loft_widget_init(&sc->w, SCALE, true, true, SIZE_STATIC);

  loft_connect(&sc->w.obj, "button-press", _scale_on_button_press, NULL);
  loft_connect(&sc->w.obj, "drag-motion", _scale_on_drag_motion, NULL);
  loft_connect(&sc->w.obj, "drag-release", _scale_on_drag_release, NULL);
  loft_connect(&sc->w.obj, "draw", _scale_on_draw, NULL);
  loft_connect(&sc->w.obj, "pointer-leave", _scale_on_pointer_leave, NULL);
  loft_connect(&sc->w.obj, "pointer-motion", _scale_on_pointer_motion, NULL);
  loft_connect(&sc->w.obj, "resize", _scale_on_resize, NULL);
  loft_connect(&sc->w.obj, "set-pref-size", _scale_on_set_pref_size, NULL);

  sc->aspect = aspect;
  sc->fill_div = 2;
  sc->inverted = false;
  sc->step = 2.0;

  sc->adj = NULL;
  sc->sl_prelight = false;

  _scale_adjust_min_size(sc);

  sc->pointer = (struct xy) { -1,-1 };
  loft_scale_set_adj(sc, adj);
}

bool loft_scale_is_inverted (LoftScale *sc) {
  return sc->inverted;
}

void loft_scale_set_adj (LoftScale *sc, LoftAdjustment *adj) {
  if(sc->adj == adj)
    return;

  if(sc->adj != NULL)
    loft_disconnect(&sc->adj->obj, sc->_adj_on_changed);

  sc->adj = adj;
  loft_emit(&sc->w.obj, "set-adjustment", sc->adj);

  if(sc->adj != NULL)
    sc->_adj_on_changed = loft_connect(&adj->obj, "changed", _scale_adj_on_changed, sc);
  else
    sc->_adj_on_changed = NULL;

  _scale_update_pos(sc);
  loft_widget_refresh(&sc->w, false);
}

void loft_scale_set_aspect (LoftScale *sc, int aspect) {
  if(sc->aspect == aspect)
    return;

  sc->aspect = aspect;
  loft_emit(&sc->w.obj, "set-aspect", &sc->aspect);

  _scale_adjust_min_size(sc);
  _scale_invert_pref_size(sc);

  loft_widget_refresh(&sc->w, false);
}

void loft_scale_set_inverted (LoftScale *sc, bool inverted) {
  if(sc->inverted == inverted)
    return;

  sc->inverted = inverted;
  loft_emit(&sc->w.obj, "set-inverted", &sc->inverted);

  _scale_update_pos(sc);
  _scale_update_sl_geo(sc);

  loft_widget_refresh(&sc->w, false);
}

void loft_scale_set_step (LoftScale *sc, double step) {
  if(sc->step == step)
    return;

  sc->step = step;
  loft_emit(&sc->w.obj, "set-step", &sc->step);

  loft_widget_refresh(&sc->w, false);
}
