#include "loft.h"
#include <math.h>

char *SWITCH = "loft-switch";

void _switch_adjust_size (LoftSwitch *sw) {
  LoftWidget *w = &sw->w;

  if(sw->type == SWITCH_FANCY) {
    if(w->pref_h > w->pref_w)
      w->pref_w = w->pref_h * 2;
    else if(w->pref_w / 2 >= ABS_MIN_SIZE) {
      w->pref_h = w->pref_w / 2;
    }
    else {
      w->pref_w = ABS_MIN_SIZE * 2;
      w->pref_h = ABS_MIN_SIZE;
    }
  }
  else if(w->pref_w > w->pref_h) {
    w->pref_h = w->pref_w;
  }
  else if(w->pref_h > w->pref_w) {
    w->pref_w = w->pref_h;
  }
}

void _switch_update_radio_group (LoftSwitch *sw) {
  LoftSwitch *gs;
  for(gs = sw->head; gs != NULL; gs = gs->next) {
    if(gs == sw || gs->type != SWITCH_RADIO || gs->active == false)
      continue;

    gs->active = false;
    gs->w.dirty = true;
    gs->w.stale = true;

    loft_emit(&gs->w.obj, "set-active", &gs->active);
  }
}

int _switch_on_button_press (LoftSwitch *sw, void *arg, void *data) {
  if(sw->type != SWITCH_FANCY)
    loft_widget_refresh_fast(&sw->w);

  return 0;
}

int _switch_on_button_release (LoftSwitch *sw, LoftButtonEvent *be, void *data) {
  if(sw->type == SWITCH_RADIO) {
    if(sw->active == false)
      _switch_update_radio_group(sw);
  }
  else if(sw->type == SWITCH_FANCY) {
    if(be->button <= 3) {
      int rx = be->x - sw->w.x;
      int half = sw->w.width / 2;
      if(rx >= half) {
        if(sw->active)
          return 0;
      }
      else if(rx <= half) {
        if(sw->active == false)
          return 0;
      }
    }
    else {
      if(be->button == 5) {
        if(sw->active)
          return 0;
      }
      else if(be->button == 4) {
        if(sw->active == false)
          return 0;
      }
    }
  }

  sw->active ^= true;
  loft_emit(&sw->w.obj, "set-active", &sw->active);
  loft_widget_refresh_fast(&sw->w);

  return 0;
}

int _switch_on_cursor_enter (LoftSwitch *sw, void *arg, void *data) {
  loft_widget_refresh_fast(&sw->w);
  return 0;
}

int _switch_on_cursor_leave (LoftSwitch *sw, void *arg, void *data) {
  loft_widget_refresh_fast(&sw->w);
  return 0;
}

int _switch_on_draw (LoftSwitch *sw, cairo_t *cr, void *data) {
  LoftWidget *w = &sw->w;

  int state;
  if(w->sensitive)
    state = NORMAL;
  else
    state = INSENSITIVE;

  double line_w = 1.2;
  cairo_set_line_width(cr, line_w);

  if(sw->type == SWITCH_FANCY) {
    struct rgba_pair *p_off;
    struct rgba_pair *p_on;

    if(sw->active) {
      p_off = &w->colors[state][IDLE];
      p_on = &loft.colors.urgency[state][LOW];
    }
    else {
      p_off = &loft.colors.urgency[state][HIGH];
      p_on = &w->colors[state][IDLE];
    }

    int padding = 5;
    int half = sw->w.width / 2;

    double x1 = padding + 0.5;
    double x2 = half - padding - 0.5;
    double yc = (sw->w.height / 2) + 0.5;

    rgba_cairo_set(cr, &p_off->bg);
    cairo_rectangle(cr, 0, 0, half, sw->w.height);
    cairo_fill(cr);

    rgba_cairo_set(cr, &p_off->fg);
    cairo_move_to(cr, x1, yc);
    cairo_line_to(cr, x2, yc);
    cairo_stroke(cr);

    double xc = half + (half / 2);
    double radius = (half - (padding * 2)) / 2;

    rgba_cairo_set(cr, &p_on->bg);
    cairo_rectangle(cr, half, 0, half, sw->w.height);
    cairo_fill(cr);

    rgba_cairo_set(cr, &p_on->fg);
    cairo_arc(cr, xc, yc, radius, 0.0, 360.0);
    cairo_stroke(cr);
  }
  else {
    int pair;
    if(sw->w.pressed)
      pair = SELECTED;
    else if(sw->w.prelight)
      pair = PRELIGHT;
    else
      pair = IDLE;

    if(sw->type == SWITCH_CHECK) {
      rgba_cairo_set(cr, &sw->w.colors[state][pair].bg);
      loft_cairo_solid_rectangle(cr, 0, 0, sw->w.width, sw->w.height);
      cairo_stroke(cr);

      if(sw->active) {
        int fill_w = sw->w.width - 6;
        int fill_h = sw->w.height - 6;
        double xo = (sw->w.width - fill_w) / 2;
        double yo = (sw->w.height - fill_h) / 2;

        rgba_cairo_set(cr, &sw->w.colors[state][SELECTED].bg);
        cairo_rectangle(cr, xo, yo, fill_w, fill_h);
        cairo_fill(cr);
      }
    }
    else { // s->type == SWITCH_RADIO
      double xc = sw->w.width / 2;
      double yc = sw->w.height / 2;

      int min = MIN(sw->w.width, sw->w.height);
      int border_radius = (min - line_w) / 2;
      int fill_radius = border_radius - 3;

      rgba_cairo_set(cr, &sw->w.colors[state][pair].bg);
      cairo_arc(cr, xc, yc, border_radius, 0.0, 360.0);
      cairo_stroke(cr);

      if(sw->active) {
        rgba_cairo_set(cr, &sw->w.colors[state][SELECTED].bg);
        cairo_arc(cr, xc, yc, fill_radius, 0.0, 360.0);
        cairo_fill(cr);
      }
    }
  }

  return 0;
}

int _switch_on_set_pref_size (LoftSwitch *sw, void *arg, void *data) {
  _switch_adjust_size(sw);
  return 0;
}

// --

#define FANCY_MIN_SIZE_CHECK \
  if(sw->type == SWITCH_FANCY) { \
    sw->w.min_w = 40; \
    sw->w.min_h = 20; \
  }

#define MIN_SIZE_CHECK \
  FANCY_MIN_SIZE_CHECK \
else { \
  sw->w.min_w = ABS_MIN_SIZE; \
  sw->w.min_h = ABS_MIN_SIZE; \
}

void loft_switch_init (LoftSwitch *sw, int type) {
  loft_widget_init(&sw->w, SWITCH, true, true, SIZE_STATIC);
  loft_connect(&sw->w.obj, "button-press", _switch_on_button_press, NULL);
  loft_connect(&sw->w.obj, "button-release", _switch_on_button_release, NULL);
  loft_connect(&sw->w.obj, "cursor-enter", _switch_on_cursor_enter, NULL);
  loft_connect(&sw->w.obj, "cursor-leave", _switch_on_cursor_leave, NULL);
  loft_connect(&sw->w.obj, "draw", _switch_on_draw, NULL);
  loft_connect(&sw->w.obj, "set-pref-size", _switch_on_set_pref_size, NULL);

  sw->type = type;
  sw->active = false;

  FANCY_MIN_SIZE_CHECK;

  sw->head = NULL;
  sw->next = NULL;
}

void loft_switch_attach (LoftSwitch *sw, LoftSwitch *ns) {
  while(sw->next != NULL)
    sw = sw->next;
  sw->next = ns;
}

void loft_switch_set_active (LoftSwitch *sw, bool active) {
  if(sw->type == SWITCH_RADIO) {
    if(active == false)
      return;

    _switch_update_radio_group(sw);
  }

  sw->active = active;
  loft_emit(&sw->w.obj, "set-active", &sw->active);

  loft_widget_refresh(&sw->w, false);
}

void loft_switch_set_type (LoftSwitch *sw, int type) {
  sw->type = type;

  MIN_SIZE_CHECK;

  if(sw->type == SWITCH_RADIO)
    _switch_update_radio_group(sw);

  _switch_adjust_size(sw);
  loft_widget_refresh(&sw->w, true);
}
