#include "loft.h"

void loft_text_tag_init (LoftTextTag *t, unsigned int attrs) {
  t->attrs = attrs;

  t->font_desc = NULL;
  t->family = NULL;

  t->background = (struct rgba) { 0,0,0,0 };
  t->foreground = (struct rgba) { 0,0,0,0 };

  t->size = 0;
  t->weight = 0;
  t->style = 0;
  t->underline = 0;
  t->underline_color = (struct rgba) { 0,0,0,0 };
  t->strikethrough = 0;
  t->strikethrough_color = (struct rgba) { 0,0,0,0 };

  t->variant = 0;
  t->letter_spacing = 0;
  t->stretch = 0;
  t->scale = 0;
  t->rise = 0;

  t->gravity = 0;
  t->gravity_hint = 0;
}
