#include "loft.h"

char *TEXT_TAG_TABLE = "loft-text-tag-table";

bool _text_tag_data_add_range (LoftTextTagData *td, int start, int end) {
  if(start < 0 || end < 0)
    return false;

  int i;
  struct range *r;

  for(i = 0; i < td->n_ranges; ++i) {
    r = &td->ranges[i];
    int exp_mask = (start < r->start && end >= r->start) |
                   (end > r->end && start <= r->end) << 1;

    if(exp_mask > 0) {
      if(exp_mask & 1)
        r->start = start;
      if(exp_mask & 2)
        r->end = end;

      return true;
    }
  }

  if(td->n_ranges == td->n_alloc_ranges) {
    size_t new_alloc_size = td->n_alloc_ranges + ALLOC_POOL;
    r = realloc(td->ranges, sizeof(struct range) * new_alloc_size);
    if(r == NULL)
      return false;

    td->ranges = r;
    td->n_alloc_ranges = new_alloc_size;
  }

  td->ranges[td->n_ranges++] = (struct range) { start, end };
  return true;
}

void _text_tag_data_clear (LoftTextTagData *td) {
  if(td->n_ranges > 0) {
    free(td->ranges);
    td->ranges = NULL;
    td->n_ranges = 0;
  }
}

bool _text_tag_data_clear_range (LoftTextTagData *td, int start, int end) {
  int i;
  struct range *r;

  for(i = 0; i < td->n_ranges; ++i) {
    r = &td->ranges[i];
    if(end < r->start || start > r->end)
      continue;

    int lo_mask = (start >= 0 && start > r->start) |
                  (end >= 0 && end < r->end) << 1;

    //  remove
    if(lo_mask == 0) {
      --td->n_ranges;
      for(; i < td->n_ranges; ++i)
        td->ranges[i] = td->ranges[i + 1];
    }
    //  adjust
    else if(lo_mask == 1) {
      r->end = start - 1;
    }
    else if(lo_mask == 2) {
      r->start = end + 1;
    }
    //  adjust, add leftover
    else if(lo_mask == 3) {
      _text_tag_data_add_range(td, end + 1, r->end);
      r->end = start - 1;
    }

    return true;
  }

  return false;
}

int _text_tag_table_on_deinit (LoftTextTagTable *tt, void *arg, void *data) {
  if(tt->len > 0)
    free(tt->data);

  return 0;
}

// --

bool loft_text_tag_table_add_tag (LoftTextTagTable *tt, LoftTextTag *t) {
  LoftTextTagData *td;

  int i;
  for(i = 0; i < tt->len; ++i) {
    td = &tt->data[i];
    if(td->tag == t)
      return true;
  }

  if(tt->len + 1 > tt->alloc_len) {
    td = realloc(tt->data, sizeof(LoftTextTagData) * (tt->alloc_len + ALLOC_POOL));
    if(td == NULL)
      return false;

    tt->data = td;
    tt->alloc_len += ALLOC_POOL;
  }

  td = &tt->data[tt->len++];
  td->tag = t;
  td->ranges = NULL;
  td->n_ranges = 0;
  td->n_alloc_ranges = 0;

  loft_emit(&tt->obj, "add-tag", td);
  return true;
}

bool loft_text_tag_table_clear_range (LoftTextTagTable *tt, LoftTextTag *t, int start, int end) {
  int i;

  if(t == NULL) {
    for(i = 0; i < tt->len; ++i)
      _text_tag_data_clear_range(&tt->data[i], start, end);

    loft_emit(&tt->obj, "changed", NULL);
  }
  else {
    LoftTextTagData *td;
    for(i = 0; i < tt->len; ++i) {
      td = &tt->data[i];
      if(td->tag == t) {
        _text_tag_data_clear_range(td, start, end);
        loft_emit(&tt->obj, "changed", td);
        break;
      }
    }
  }

  return true;
}

bool loft_text_tag_table_clear_tag (LoftTextTagTable *tt, LoftTextTag *t) {
  int i;
  LoftTextTagData *td;

  for(i = 0; i < tt->len; ++i) {
    td = &tt->data[i];
    if(td->tag == t) {
      _text_tag_data_clear(td);
      loft_emit(&tt->obj, "changed", td);
      return true;
    }
  }

  return false;
}

void loft_text_tag_table_init (LoftTextTagTable *tt) {
  loft_object_init(&tt->obj, TEXT_TAG_TABLE);
  loft_connect(&tt->obj, "deinit", _text_tag_table_on_deinit, NULL);

  tt->data = NULL;
  tt->len = 0;
  tt->alloc_len = 0;
}

bool loft_text_tag_table_rm_tag (LoftTextTagTable *tt, LoftTextTag *t) {
  int i;
  LoftTextTagData *td;

  for(i = 0; i < tt->len; ++i) {
    td = &tt->data[i];
    if(td->tag == t) {
      if(td->n_ranges > 0)
        free(td->ranges);

      --tt->len;
      for(; i < tt->len; ++i)
        tt->data[i] = tt->data[i + 1];

      loft_emit(&tt->obj, "changed", NULL);
      return true;
    }
  }

  return false;
}

bool loft_text_tag_table_set_range (LoftTextTagTable *tt, LoftTextTag *t, int start, int end) {
  int i;
  LoftTextTagData *td;

  for(i = 0; i < tt->len; ++i) {
    td = &tt->data[i];
    if(td->tag == t) {
      if(_text_tag_data_add_range(td, start, end) == false)
        return false;

      loft_emit(&tt->obj, "set-range", t);
      loft_emit(&tt->obj, "changed", NULL);

      return true;
    }
  }

  return false;
}

PangoAttrList * loft_text_tag_table_to_attr_list (LoftTextTagTable *tt) {
  PangoAttrList *attrs = pango_attr_list_new();

  if(tt == NULL)
    return attrs;

  int i,j;
  LoftTextTagData *td;
  LoftTextTag *t;
  struct range *r;

  PangoAttribute *attr;
  PangoFontDescription *d;
  struct rgba_values cv;

  for(i = 0; i < tt->len; ++i) {
    td = &tt->data[i];
    t = td->tag;

    for(j = 0; j < td->n_ranges; ++j) {
      r = &td->ranges[j];

      if(t->attrs & ATTR_FONT_DESCRIPTION) {
        d = pango_font_description_from_string(t->font_desc);
        attr = pango_attr_font_desc_new(d);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_FAMILY) {
        attr = pango_attr_family_new(t->family);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_BACKGROUND) {
        rgba_values(&t->background, &cv, RGBA_16);
        attr = pango_attr_background_new(cv.r, cv.g, cv.b);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_FOREGROUND) {
        rgba_values(&t->foreground, &cv, RGBA_16);
        attr = pango_attr_foreground_new(cv.r, cv.g, cv.b);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_SIZE) {
        attr = pango_attr_size_new(t->size);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_WEIGHT) {
        attr = pango_attr_weight_new(t->weight);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_STYLE) {
        attr = pango_attr_style_new(t->style);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_UNDERLINE) {
        attr = pango_attr_underline_new(t->underline);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_UNDERLINE_COLOR) {
        rgba_values(&t->underline_color, &cv, RGBA_16);
        attr = pango_attr_underline_color_new(cv.r, cv.g, cv.b);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_STRIKETHROUGH) {
        attr = pango_attr_strikethrough_new(t->strikethrough);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_STRIKETHROUGH_COLOR) {
        rgba_values(&t->strikethrough_color, &cv, RGBA_16);
        attr = pango_attr_strikethrough_color_new(cv.r, cv.g, cv.b);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_VARIANT) {
        attr = pango_attr_variant_new(t->variant);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_LETTER_SPACING) {
        attr = pango_attr_letter_spacing_new(t->letter_spacing);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_STRETCH) {
        attr = pango_attr_stretch_new(t->stretch);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_SCALE) {
        attr = pango_attr_scale_new(t->scale);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_RISE) {
        attr = pango_attr_rise_new(t->rise);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_GRAVITY) {
        attr = pango_attr_gravity_new(t->gravity);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }

      if(t->attrs & ATTR_GRAVITY_HINT) {
        attr = pango_attr_gravity_hint_new(t->gravity_hint);
        attr->start_index = r->start;
        attr->end_index = r->end;
        pango_attr_list_insert(attrs, attr);
      }
    }
  }

  return attrs;
}
