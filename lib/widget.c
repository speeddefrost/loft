#include "loft.h"
#include <cairo/cairo-xlib.h>

extern void _arrange (LoftWidget *w);
extern void _redraw (LoftWidget *w);  // -- refresh.c
extern void _repaint_children (LoftWidget *w, LoftWidget *n, cairo_t *cr);

void _widget_attach (LoftWidget *w, LoftWidget *p, int pos) {
  debug(B_GREEN "ATTACH " C_ESC B_WHITE "%s" C_ESC " @ " B_WHITE "%p" C_ESC " to " B_WHITE "%s" C_ESC " @ " B_WHITE "%p\n" C_ESC, w->obj.type, w, p->obj.type, p);

  loft_emit(&p->obj, "pre-add-child", w);
  loft_emit(&w->obj, "pre-set-parent", p);

  if(pos == -1)
    pos = p->n_children;
  else if(pos < 0)
    pos = p->n_children + pos + 1;
  else if(pos > p->n_children)
    pos = p->n_children;

  if(pos == 0) {
    w->next = p->child;
    p->child = w;
  }
  else {
    LoftWidget *ps = p->child;

    int i;
    for(i = 1; i < pos; ++i)
      ps = ps->next;

    w->next = ps->next;
    ps->next = w;
  }

  w->parent = p;
  ++p->n_children;

  loft_emit(&p->obj, "add-child", w);
  loft_emit(&w->obj, "set-parent", p);
}

void _widget_unfocus_child (LoftWidget *w, LoftWindow *win) {
  if(w == win->focused) {
    win->focused = NULL;
    w->has_focus = false;
    w->has_input_focus = false;
    return;
  }

  for(w = w->child; w != NULL; w = w->next)
    _widget_unfocus_child(w, win);
}

void _widget_detach (LoftWidget *w, LoftWidget *p) {
  debug(B_BLUE "DETACH " C_ESC B_WHITE "%s" C_ESC " @ " B_WHITE "%p" C_ESC " from " B_WHITE "%s" C_ESC " @ " B_WHITE "%p\n" C_ESC, w->obj.type, w, p->obj.type, p);

  loft_emit(&p->obj, "pre-rm-child", w);
  loft_emit(&w->obj, "pre-set-parent", NULL);

  if(p->child != w) {
    LoftWidget *s;
    for(s = p->child; s != NULL; s = s->next) {
      if(s->next == w) {
        s->next = w->next;
        break;
      }
    }
  }
  else {
    p->child = w->next;
  }

  w->next = NULL;
  w->parent = NULL;

  --p->n_children;

  loft_emit(&p->obj, "rm-child", w);
  loft_emit(&w->obj, "set-parent", NULL);
}

void _widget_first_child_at_xy (LoftWidget *w, struct xy *p, LoftWidget **c) {
  if(loft_xy_within(p, &w->geo)) {
    *c = w;
    return;
  }

  for(w = w->child; w != NULL; w = w->next)
    _widget_first_child_at_xy(w, p, c);
}

void _widget_invalidate (LoftWidget *w) {
  LoftWidget *n = w->parent;
  struct xywh *clip = NULL;

  while(true) {
    if(n == NULL || n->visible == false)
      return;

    if(n->obj.type == WINDOW)
      break;

    if(n->can_clip && clip == NULL)
      clip = &n->clip;

    n = n->parent;
  }

  if(n->redraw)
    return;

  LoftWindow *win = (LoftWindow*) n;
  debug(B_YELLOW "INVALIDATE " C_ESC B_WHITE "%s" C_ESC " @ " B_WHITE "%p\n" C_ESC, w->obj.type, w);

  if(win->_n_stale == win->_n_alloc_stale) {
    struct xywh *tmp = realloc (
        win->_stale,
        sizeof(struct xywh) * (win->_n_alloc_stale + ALLOC_POOL)
        );

    if(tmp == NULL)
      return;

    win->_stale = tmp;
    win->_n_alloc_stale += ALLOC_POOL;
  }

  struct xywh *area = &win->_stale[win->_n_stale++];
  *area = (struct xywh) { w->x, w->y, w->width, w->height };

  if(clip != NULL)
    loft_xywh_clip(area, clip);
}

void _widget_hide_all (LoftWidget *w) {
  if(w->visible) {
    w->stale = false;
    w->visible = false;

    LoftObject *obj = &w->obj;
    loft_emit(obj, "set-visible", &w->visible);
    loft_emit(obj, "hide", NULL);
  }

  for(w = w->child; w != NULL; w = w->next)
    _widget_hide_all(w);
}

void _widget_propagate_sensitivity (LoftWidget *w) {
  if(w->sensitive != w->parent->sensitive) {
    w->dirty = true;
    w->sensitive = w->parent->sensitive;
    loft_emit(&w->obj, "set-sensitive", &w->sensitive);
  }

  for(w = w->child; w != NULL; w = w->next)
    _widget_propagate_sensitivity(w);
}

void _widget_set_stale (LoftWidget *w) {
  w->stale = true;
  for(w = w->child; w != NULL; w = w->next)
    _widget_set_stale(w);
}

void _widget_show_all (LoftWidget *w) {
  if(w->visible == false) {
    w->stale = true;
    w->visible = true;

    LoftObject *obj = &w->obj;
    loft_emit(obj, "set-visible", &w->visible);
    loft_emit(obj, "show", NULL);
  }

  for(w = w->child; w != NULL; w = w->next)
    _widget_show_all(w);
}

void _widget_update_clip (LoftWidget *w) {
  w->clip = (struct xywh) {
    w->x + w->clip_adj.x,
      w->y + w->clip_adj.y,
      w->width - w->clip_adj.width,
      w->height - w->clip_adj.height
  };

  w->dirty_clip = false;
}

int _widget_on_destroy (LoftWidget *w, void *arg, void *data) {
  while(w->child != NULL)
    loft_object_destroy(&w->child->obj);

  if(w->parent != NULL)
    _widget_detach(w, w->parent);

  cairo_surface_destroy(w->buffer);
  return 0;
}

// --

void loft_widget_attach (LoftWidget *w, LoftWidget *p, int pos) {
  if(w->parent == p)
    return;

  loft_widget_detach(w);
  _widget_attach(w, p, pos);
  loft_queue_refresh(p, true);
}

void loft_widget_base_size (LoftWidget *w, int *base_w, int *base_h) {
  if(base_w != NULL)
    *base_w = MAX(w->pref_w, w->min_w);
  if(base_h != NULL)
    *base_h = MAX(w->pref_h, w->min_h);
}

bool loft_widget_can_clip (LoftWidget *w) {
  return w->can_clip;
}

bool loft_widget_can_focus (LoftWidget *w) {
  return w->can_focus;
}

void loft_widget_configure (LoftWidget *w, int x, int y, int width, int height) {
  int diff = (x != w->x || y != w->y) |
    ((width != w->width || height != w->height) << 1);

  if(diff == 0)
    return;

  if(w->visible && w->stale == false) {
    _widget_invalidate(w);
    w->stale = true;
  }

  if(diff & 1) {
    w->old_x = w->x;
    w->old_y = w->y;
    w->x = x;
    w->y = y;

    loft_emit(&w->obj, "move", NULL);
  }

  if(diff & 2) {
    w->dirty = true;

    w->old_w = w->width;
    w->old_h = w->height;
    w->width = width;
    w->height = height;

    loft_widget_grow_buffer(w, width, height);

    loft_emit(&w->obj, "resize", NULL);
  }

  _widget_update_clip(w);
  loft_emit(&w->obj, "configure", NULL);
}

void loft_widget_detach (LoftWidget *w) {
  LoftWidget *p = w->parent;
  if(p == NULL)
    return;

  if(w->visible && w->stale == false)
    _widget_invalidate(w);

  LoftWindow *win;
  loft_widget_native(w, (LoftWidget**) &win);
  if(win != NULL && win->focused != NULL)
    _widget_unfocus_child(w, win);

  _widget_detach(w,p);
  loft_queue_refresh(p, true);
}

void loft_widget_detach_children (LoftWidget *w) {
  LoftWidget *next;
  LoftWidget *c = w->child;

  while(c != NULL) {
    c->parent = NULL;
    next = c->next;
    c->next = NULL;
    c = next;
  }

  w->child = NULL;

  if(w->visible && w->stale == false)
    _widget_invalidate(w);

  loft_queue_refresh(w, true);
}

void loft_widget_focus (LoftWidget *w) {
  if(w->can_focus == false || w->has_focus)
    return;

  LoftWindow *win;
  loft_widget_native(w, (LoftWidget**) &win);
  if(win == NULL || w == win->focused)
    return;

  if(win->focused != NULL) {
    LoftWidget *f = win->focused;

    f->has_focus = false;
    f->has_input_focus = false;

    loft_emit(&f->obj, "set-focus", &f->has_focus);
    loft_emit(&f->obj, "set-input-focus", &f->has_input_focus);

    f->dirty = true;
  }

  win->focused = w;

  w->has_focus = true;
  w->has_input_focus = win->w.has_focus;

  loft_emit(&w->obj, "set-focus", &w->has_focus);
  loft_emit(&w->obj, "set-input-focus", &w->has_input_focus);

  loft_widget_refresh(w, true);
}

unsigned int loft_widget_get_attrs (LoftWidget *w) {
  return w->attrs;
}

LoftWidget *loft_widget_get_child_at_xy (LoftWidget *w, struct xy *p) {
  LoftWidget *c = NULL;
  for(w = w->child; w != NULL; w = w->next) {
    _widget_first_child_at_xy(w, p, &c);
    if(c != NULL)
      break;
  }
  return c;
}

int loft_widget_grow_buffer (LoftWidget *w, int width, int height) {
  uint8_t diff = (width > w->buf_w) << 0 |
    (height > w->buf_h) << 1;

  if(diff == 0)
    return 1;

  if(diff & 1)
    w->buf_w = width;
  if(diff & 2)
    w->buf_h = height;

  if(w->buffer != NULL)
    cairo_surface_destroy(w->buffer);

  w->buffer = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w->buf_w, w->buf_h);
  if(w->buffer == NULL)
    return -1;

  return 0;
}

bool loft_widget_has_focus (LoftWidget *w) {
  return w->has_focus;
}

bool loft_widget_has_input_focus (LoftWidget *w) {
  return w->has_input_focus;
}

void loft_widget_hide (LoftWidget *w) {
  if(w->visible == false || w->obj.type == WINDOW)
    return;

  if(w->stale == false)
    _widget_invalidate(w);
  else
    w->stale = false;

  w->visible = false;

  loft_emit(&w->obj, "set-visible", &w->visible);
  loft_emit(&w->obj, "hide", NULL);

  loft_queue_refresh(w, true);
}

void loft_widget_hide_all (LoftWidget *w) {
  if(w->obj.type == WINDOW)
    return;

  if(w->visible && w->stale == false)
    _widget_invalidate(w);

  _widget_hide_all(w);
  loft_queue_refresh(w, true);
}

void loft_widget_init (LoftWidget *w, char *type, bool visible, bool drawable, int size_mode) {
  loft_object_init(&w->obj, type);
  loft_connect(&w->obj, "destroy", _widget_on_destroy, NULL);

  w->attrs = 0;
  w->size_mode = size_mode;

  w->dirty = drawable;
  w->stale = true;
  w->x = 0;
  w->y = 0;
  w->old_x = -1;
  w->old_y = -1;
  w->width = ABS_MIN_SIZE;
  w->height = ABS_MIN_SIZE;
  w->buf_w = w->width;
  w->buf_h = w->height;
  w->old_w = -1;
  w->old_h = -1;
  w->min_w = w->width;
  w->min_h = w->height;
  w->pref_w = -1;
  w->pref_h = -1;

  w->drawable = drawable;

  w->dragging = false;
  w->prelight = false;
  w->pressed = false;
  w->sensitive = true;
  w->visible = visible;

  w->can_clip = false;
  w->dirty_clip = false;

  w->can_focus = false;
  w->has_focus = false;
  w->has_input_focus = false;

  w->child = NULL;
  w->next = NULL;
  w->parent = NULL;
  w->n_children = 0;

  w->clip = (struct xywh) { w->x, w->y, w->width, w->height };
  w->clip_adj = (struct xywh) { 0,0,0,0 };

  w->buffer = cairo_image_surface_create(CAIRO_FORMAT_ARGB32, w->buf_w, w->buf_h);
  memcpy(&w->colors, &loft.colors.common, sizeof(loft.colors.common));
}

bool loft_widget_is_drawable (LoftWidget *w) {
  return w->drawable;
}

bool loft_widget_is_dirty (LoftWidget *w) {
  return w->dirty;
}

bool loft_widget_is_prelight (LoftWidget *w) {
  return w->prelight;
}

bool loft_widget_is_pressed (LoftWidget *w) {
  return w->pressed;
}

bool loft_widget_is_sensitive (LoftWidget *w) {
  return w->sensitive;
}

bool loft_widget_is_set_visible (LoftWidget *w) {
  return w->visible;
}

bool loft_widget_is_stale (LoftWidget *w) {
  return w->stale;
}

bool loft_widget_is_visible (LoftWidget *w) {
  while(w != NULL) {
    if(w->visible == false)
      return false;

    if(w->obj.type == WINDOW)
      return true;

    w = w->parent;
  }

  return false;
}

bool loft_widget_native (LoftWidget *w, LoftWidget **n) {
  bool vtrail = true;

  while(w != NULL) {
    if(w->obj.type == WINDOW) {
      *n = w;
      return vtrail;
    }

    if(vtrail && w->visible == false)
      vtrail = false;

    w = w->parent;
  }

  *n = NULL;
  return false;
}

void loft_widget_refresh (LoftWidget *w, bool size_update) {
  if(w->visible == false)
    return;

  w->dirty = true;
  loft_queue_refresh(w, size_update);
}

void loft_widget_refresh_fast (LoftWidget *w) {
  if(loft.state != RUNNING || w->visible == false || w->drawable == false)
    return;

  LoftWidget *n = w->parent;
  struct xywh *p_clip = NULL;

  while(true) {
    if(n == NULL || n->visible == false)
      return;

    if(n->obj.type == WINDOW)
      break;

    if(n->can_clip && p_clip == NULL)
      p_clip = &n->clip;

    n = n->parent;
  }

  loft_lock();

  _widget_set_stale(w);
  _arrange(n);

  LoftWindow *win = (LoftWindow*) n;
  cairo_t *cr = cairo_create(win->target);

  struct xy *n_pos;
  if(p_clip == NULL) {
    n_pos = &w->pos;
  }
  else {
    n_pos = (struct xy*) p_clip;
    cairo_rectangle (
        cr,
        p_clip->x, p_clip->y,
        p_clip->width, p_clip->height
        );
    cairo_clip(cr);
  }

  cairo_set_operator(cr, CAIRO_OPERATOR_SOURCE);
  cairo_set_source_surface(cr, n->buffer, n_pos->x, n_pos->y);
  cairo_rectangle(cr, w->x, w->y, w->width, w->height);
  cairo_fill(cr);

  w->dirty = true;

  cairo_set_operator(cr, CAIRO_OPERATOR_OVER);
  _repaint_children(w, n, cr);
  cairo_destroy(cr);

  loft_unlock();
}

void loft_widget_set_attrs (LoftWidget *w, unsigned int attrs) {
  w->attrs = attrs;
  loft_emit(&w->obj, "set-attrs", &w->attrs);
  loft_widget_refresh(w, true);
}

void loft_widget_set_drawable (LoftWidget *w, bool drawable) {
  if(w->drawable == drawable)
    return;

  w->dirty = drawable;
  w->drawable = drawable;

  loft_widget_refresh(w, false);
}

void loft_widget_set_min_size (LoftWidget *w, int min_w, int min_h) {
  w->min_w = min_w;
  w->min_h = min_h;
}

void loft_widget_set_pref_size (LoftWidget *w, int pref_w, int pref_h) {
  w->pref_w = pref_w;
  w->pref_h = pref_h;
  loft_emit(&w->obj, "set-pref-size", &w->pref_size);
  loft_widget_refresh(w, true);
}

void loft_widget_set_sensitive (LoftWidget *w, bool sensitive) {
  if(w->parent != NULL && w->parent->sensitive != sensitive)
    return;

  w->dirty = true;
  w->sensitive = sensitive;
  _widget_propagate_sensitivity(w);

  loft_queue_refresh(w, false);
}

void loft_widget_show (LoftWidget *w) {
  if(w->visible || w->obj.type == WINDOW)
    return;

  w->stale = true;
  w->visible = true;

  loft_emit(&w->obj, "set-visible", &w->visible);
  loft_emit(&w->obj, "show", NULL);

  loft_queue_refresh(w, true);
}

void loft_widget_show_all (LoftWidget *w) {
  if(w->obj.type == WINDOW)
    return;

  _widget_show_all(w);
  loft_queue_refresh(w, true);
}

bool loft_widget_touching (LoftWidget *w, struct xy *p) {
  LoftWidget *wp;
  struct xywh *c = NULL;

  for(wp = w->parent; wp != NULL; wp = wp->parent) {
    if(wp->can_clip) {
      c = &wp->clip;
      break;
    }
  }

  if(c == NULL)
    return loft_xy_within(p, &w->geo);
  else
    return loft_xy_within(p, c);
}

void loft_widget_unfocus (LoftWidget *w) {
  LoftWindow *win;
  loft_widget_native(w, (LoftWidget**) &win);
  if(win == NULL || w != win->focused)
    return;

  win->focused = NULL;

  w->has_focus = false;
  w->has_input_focus = false;

  loft_emit(&w->obj, "set-focus", &w->has_focus);
  loft_emit(&w->obj, "set-input-focus", &w->has_input_focus);

  loft_widget_refresh(w, true);
}
