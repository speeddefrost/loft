#include "loft.h"
#include <cairo/cairo-xlib.h>
#include <X11/Xatom.h>
#include <X11/Xutil.h>

char *WINDOW = "loft-window";

int _window_on_arrange (LoftWindow *win, void *arg, void *data) {
  if(win->w.child == NULL)
    return 0;

  LoftWidget *n = &win->w;
  LoftWidget *c = n->child;

  int base_w,base_h;
  loft_widget_base_size(n->child, &base_w, &base_h);

  int cx,cy,cw,ch;
  int padding = win->padding * 2;

  if(c->attrs & EXPAND_X) {
    if(c->attrs & PACK_X) {
      cw = base_w;
      if(c->attrs & FLOW_L)
        cx = win->padding;
      else if(c->attrs & FLOW_R)
        cx = n->width - win->padding - base_w;
      else
        cx = (n->width - base_w) / 2;
    }
    else {
      cw = n->width - padding;
      cx = win->padding;
    }
  }
  else {
    cw = base_w;
    cx = (n->width - base_w) / 2;
  }

  if(c->attrs & EXPAND_Y) {
    if(c->attrs & PACK_Y) {
      ch = base_h;
      if(c->attrs & FLOW_U)
        cy = win->padding;
      else if(c->attrs & FLOW_D)
        cy = n->height - win->padding - base_h;
      else
        cy = (n->height - base_h) / 2;
    }
    else {
      ch = n->height - padding;
      cy = win->padding;
    }
  }
  else {
    ch = base_h;
    cy = (n->height - base_h) / 2;
  }

  loft_widget_configure(n->child, cx, cy, cw, ch);
  return 0;
}

int _window_on_draw (LoftWindow *win, cairo_t *cr, void *data) {
  rgba_cairo_set(cr, &win->base_color);
  cairo_rectangle(cr, 0, 0, win->w.width, win->w.height);
  cairo_fill(cr);
  return 0;
}

int _window_on_pre_add_child (LoftWidget *n, LoftWidget *child, void *data) {
  if(n->child != NULL)
    loft_widget_detach(n->child);
  return 0;
}

int _window_on_set_pref_size (LoftWidget *n, void *arg, void *data) {
  LoftWindow *win = (LoftWindow*) n;

  int base_w, base_h;
  loft_widget_base_size(n, &base_w, &base_h);

  XSizeHints *hints = XAllocSizeHints();
  hints->flags = PBaseSize;
  hints->base_width = base_w;
  hints->base_height = base_h;
  XSetWMNormalHints(loft.display, win->xwin, hints);
  XFree(hints);

  loft_window_resize(win, base_w, base_h);
  return 0;
}

int _window_on_size_request (LoftWindow *win, struct wh *req, void *data) {
  LoftWidget *n;
  LoftWidget *c;
  struct wh r;
  int p2,
      min_w,min_h,
      base_w,base_h;

  n = &win->w;
  c = n->child;
  p2 = win->padding * 2;
  min_w = p2;
  min_h = p2;

  if(c != NULL) {
    r.height = -1;
    if(c->size_mode == SIZE_H4W)
      r.width = MAX(0, req->width - min_w);
    else
      r.width = -1;

    loft_emit(&n->child->obj, "size-request", &r);
    loft_widget_base_size(n->child, &base_w, &base_h);

    min_w += base_w;
    min_h += base_h;
  }

  if(min_w != n->min_w || min_h != n->min_h) {
    loft_widget_set_min_size(n, min_w, min_h);

    XSizeHints *hints = XAllocSizeHints();
    hints->flags = PMinSize;
    hints->min_width = n->min_w;
    hints->min_height = n->min_h;
    XSetWMNormalHints(loft.display, win->xwin, hints);
    XFree(hints);
  }

  return 0;
}

int _window_on_destroy (LoftWindow *win, void *arg, void *data) {
  if(win->_n_stale > 0)
    free(win->_stale);

  cairo_surface_destroy(win->target);
  XDestroyWindow(loft.display, win->xwin);

  return 0;
}

// --

LoftWindow *loft_window_from_xwin (Window xwin) {
  int i;
  LoftObject *obj;
  LoftWindow *win;

  for(i = 0; i < loft.objects.used_size; ++i) {
    obj = (LoftObject*) loft.objects.items[i].data;
    if(obj->type != WINDOW)
      continue;

    win = (LoftWindow*) obj;
    if(win->xwin == xwin)
      return win;
  }

  return NULL;
}

bool loft_window_get_override_redirect (LoftWindow *win) {
  XWindowAttributes wa;
  XGetWindowAttributes(loft.display, win->xwin, &wa);
  return wa.override_redirect;
}

void loft_window_hide_all (LoftWindow *win) {
  if(win->w.child != NULL)
    loft_widget_hide_all(win->w.child);

  loft_window_hide(win);
}

void loft_window_init (LoftWindow *win, char *name, int padding, bool override_redirect) {
  LoftWidget *n = &win->w;
  loft_widget_init(n, WINDOW, false, true, SIZE_H4W);

  loft_connect(&n->obj, "arrange", _window_on_arrange, NULL);
  loft_connect(&n->obj, "draw", _window_on_draw, NULL);
  loft_connect(&n->obj, "pre-add-child", _window_on_pre_add_child, NULL);
  loft_connect(&n->obj, "set-pref-size", _window_on_set_pref_size, NULL);
  loft_connect(&n->obj, "size-request", _window_on_size_request, NULL);

  loft_connect_tail(&n->obj, "destroy", _window_on_destroy, NULL);

  rgba_copy(&win->base_color, &loft.colors.base);

  XSetWindowAttributes wa = {
    .backing_store = WhenMapped,
    .border_pixel = 0,
    .colormap = loft.colormap,
    .event_mask = loft.event_mask,
    .override_redirect = override_redirect,
    .save_under = true
  };

  win->xwin = XCreateWindow (
    loft.display,
    loft.root,
    n->x, n->y,
    n->width, n->height,
    0,
    loft.depth,
    InputOutput,
    loft.visual,
    CWBackingStore|CWBorderPixel|CWColormap|CWEventMask|CWOverrideRedirect|CWSaveUnder,
    &wa
  );

  win->target = cairo_xlib_surface_create(
    loft.display,
    win->xwin,
    loft.visual,
    n->width, n->height
  );

  win->focused = NULL;
  win->padding = padding;

  XSetWMProtocols(loft.display, win->xwin, &loft.wm_delete_win, 1);

  if(name != NULL)
    loft_window_set_name(win, name);

  // --

  win->_n_stale = 0;
  win->_n_alloc_stale = 25;
  win->_stale = malloc(sizeof(struct xywh) * win->_n_alloc_stale);
}

void loft_window_set_dialog (LoftWindow *win, bool dialog) {
  if(dialog) {
    XChangeProperty(loft.display, win->xwin, loft.wm_window_type, XA_ATOM, 32,
        PropModeReplace, (unsigned char*) &loft.wm_window_type_dialog, 1);
  }
  else {
    XChangeProperty(loft.display, win->xwin, loft.wm_window_type, XA_ATOM, 32,
        PropModeReplace, (unsigned char*) 0, 0);
  }
}

void loft_window_set_fullscreen (LoftWindow *win, bool fullscreen) {
  if(fullscreen) {
    XChangeProperty(loft.display, win->xwin, loft.wm_state, XA_ATOM, 32,
        PropModeReplace, (unsigned char*) &loft.wm_state_fullscreen, 1);
  }
  else {
    XChangeProperty(loft.display, win->xwin, loft.wm_state, XA_ATOM, 32,
        PropModeReplace, (unsigned char*) 0, 0);
  }
}

void loft_window_set_padding (LoftWindow *win, int padding) {
  win->padding = padding;
  loft_widget_refresh(&win->w, true);
}

void loft_window_set_override_redirect (LoftWindow *win, bool override_redirect) {
  XSetWindowAttributes wa = { .override_redirect = override_redirect };
  XChangeWindowAttributes(loft.display, win->xwin, CWOverrideRedirect, &wa);
}

void loft_window_set_constraints (LoftWindow *win, unsigned int constraints) {
  win->constraints = constraints;
}

void loft_window_set_urgent (LoftWindow *win, bool urgent) {
  XWMHints *wmh = XGetWMHints(loft.display, win->xwin);
  if(wmh == NULL)
    return;

  if(urgent)
    wmh->flags |= XUrgencyHint;
  else
    wmh->flags &= ~XUrgencyHint;

  XSetWMHints(loft.display, win->xwin, wmh);
  XFree(wmh);
}

void loft_window_show_all (LoftWindow *win) {
  if(win->w.child != NULL)
    loft_widget_show_all(win->w.child);

  loft_window_show(win);
}
